/* 
 * File:   dex.h
 * Author: User
 *
 * Created on June 28, 2021, 2:56 PM
 */

#ifndef DEX_H
#define	DEX_H

extern const unsigned char Dollar_Array[][12];
extern unsigned int guiDex_Data_Total_Page,guiCurrentDexPage;
extern unsigned char gucCash_SalesData_Buff[2500];
extern unsigned int guiCash_SalesData_Buff_index;

extern unsigned int Machinewait_Count;
extern unsigned char gucPowerOnDEX_Flag,gucDollorD_Flag,gucDex_init_flag,gucDex_ON_flag,gucDEX_Card_Reset_Status_Flg,gucDex_Running_flag;

extern unsigned char gucDEX_DoneFlag;

extern void Product_Array_Read(unsigned int Strat_Add, unsigned char No_Page_Read);
extern void EJB_Array_Read(unsigned int Strat_Add,unsigned char No_Of_Page);
extern unsigned int toint(unsigned char str[],int slen);
extern void tostring(char str[], int num);
extern void Calculate_Compartment_Number(void);
extern void LiveCashDataUpdate(void);

extern void DEX_Communication(void);
#endif	/* DEX_H */

