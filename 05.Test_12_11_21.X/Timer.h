/* 
 * File:   Timer.h
 * Author: User
 *
 * Created on June 28, 2021, 1:04 PM
 */

#ifndef TIMER_H
#define	TIMER_H


#define SEC 10


void TMR0_Initialize(void);
void TMR0_StartTimer(void);
//void TMR0_StopTimer(void);

#endif	/* TIMER_H */

