/* 
 * File:   basic_requirements.h
 * Author: User
 *
 * Created on June 28, 2021, 12:21 PM
 */

#ifndef BASIC_REQUIREMENTS_H
#define	BASIC_REQUIREMENTS_H


#include <xc.h>
#include <math.h>
#include <string.h>
#include "BLE_Protocol.h"
#include "BLE_UART.h"
#include "MDB_Protocol.h"
#include "MDB_UART.h"
#include "interrupts.h"
#include "Timer.h"
#include "dex.h"
#include "keypad.h"
#include "I2C.h"
//------------------------------ Configuration bits Declarations ------------------------------------------------------------------------------ 
// Configuration bits: selected in the GUI

// CONFIG1H
#pragma config FOSC = XT    // Oscillator Selection bits->XT oscillator
#pragma config PLLCFG = OFF    // 4X PLL Enable->Oscillator used directly
#pragma config PRICLKEN = ON    // Primary clock enable bit->Primary clock is always enabled
#pragma config FCMEN = OFF    // Fail-Safe Clock Monitor Enable bit->Fail-Safe Clock Monitor disabled
#pragma config IESO = OFF    // Internal/External Oscillator Switchover bit->Oscillator Switchover mode disabled

// CONFIG2L
#pragma config PWRTEN = OFF    // Power-up Timer Enable bit->Power up timer disabled
#pragma config BOREN = SBORDIS    // Brown-out Reset Enable bits->Brown-out Reset enabled in hardware only (SBOREN is disabled)
#pragma config BORV = 190    // Brown Out Reset Voltage bits->VBOR set to 1.90 V nominal

// CONFIG2H
#pragma config WDTEN = OFF    // Watchdog Timer Enable bits->Watch dog timer is always disabled. SWDTEN has no effect.
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits->1:32768

// CONFIG3H
#pragma config CCP2MX = PORTC1    // CCP2 MUX bit->CCP2 input/output is multiplexed with RC1
#pragma config PBADEN = ON    // PORTB A/D Enable bit->PORTB<5:0> pins are configured as analog input channels on Reset
#pragma config CCP3MX = PORTB5    // P3A/CCP3 Mux bit->P3A/CCP3 input/output is multiplexed with RB5
#pragma config HFOFST = ON    // HFINTOSC Fast Start-up->HFINTOSC output and ready status are not delayed by the oscillator stable status
#pragma config T3CMX = PORTC0    // Timer3 Clock input mux bit->T3CKI is on RC0
#pragma config P2BMX = PORTD2    // ECCP2 B output mux bit->P2B is on RD2
#pragma config MCLRE = EXTMCLR    // MCLR Pin Enable bit->MCLR pin enabled, RE3 input pin disabled

// CONFIG4L
#pragma config STVREN = ON    // Stack Full/Underflow Reset Enable bit->Stack full/underflow will cause Reset
#pragma config LVP = OFF    // Single-Supply ICSP Enable bit->Single-Supply ICSP disabled
#pragma config XINST = OFF    // Extended Instruction Set Enable bit->Instruction set extension and Indexed Addressing mode disabled (Legacy mode)
#pragma config DEBUG = OFF    // Background Debug->Disabled

// CONFIG5L
#pragma config CP0 = OFF    // Code Protection Block 0->Block 0 (000800-003FFFh) not code-protected
#pragma config CP1 = OFF    // Code Protection Block 1->Block 1 (004000-007FFFh) not code-protected
#pragma config CP2 = OFF    // Code Protection Block 2->Block 2 (008000-00BFFFh) not code-protected
#pragma config CP3 = OFF    // Code Protection Block 3->Block 3 (00C000-00FFFFh) not code-protected

// CONFIG5H
#pragma config CPB = OFF    // Boot Block Code Protection bit->Boot block (000000-0007FFh) not code-protected
#pragma config CPD = OFF    // Data EEPROM Code Protection bit->Data EEPROM not code-protected

// CONFIG6L
#pragma config WRT0 = OFF    // Write Protection Block 0->Block 0 (000800-003FFFh) not write-protected
#pragma config WRT1 = OFF    // Write Protection Block 1->Block 1 (004000-007FFFh) not write-protected
#pragma config WRT2 = OFF    // Write Protection Block 2->Block 2 (008000-00BFFFh) not write-protected
#pragma config WRT3 = OFF    // Write Protection Block 3->Block 3 (00C000-00FFFFh) not write-protected

// CONFIG6H
#pragma config WRTC = OFF    // Configuration Register Write Protection bit->Configuration registers (300000-3000FFh) not write-protected
#pragma config WRTB = OFF    // Boot Block Write Protection bit->Boot Block (000000-0007FFh) not write-protected
#pragma config WRTD = OFF    // Data EEPROM Write Protection bit->Data EEPROM not write-protected

// CONFIG7L
#pragma config EBTR0 = OFF    // Table Read Protection Block 0->Block 0 (000800-003FFFh) not protected from table reads executed in other blocks
#pragma config EBTR1 = OFF    // Table Read Protection Block 1->Block 1 (004000-007FFFh) not protected from table reads executed in other blocks
#pragma config EBTR2 = OFF    // Table Read Protection Block 2->Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks
#pragma config EBTR3 = OFF    // Table Read Protection Block 3->Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks

// CONFIG7H
#pragma config EBTRB = OFF    // Boot Block Table Read Protection bit->Boot Block (000000-0007FFh) not protected from table reads executed in other blocks

//---------------------------------------------------------------------------------------------------------------------------------------------
//----------------------- Macro Declarations -----------------------------------------------------------

#define _XTAL_FREQ 11059200

#define TRUE	1
#define FALSE	0

//#define dDEBUG  


#define dBLE_BAUDRATE 115200

#define dBLE_RESET_PIN  LATDbits.LATD5 
#define dBLE_STATUS_PIN PORTDbits.RD3

//#define DEX_RUN_Pin                     PORTAbits.RA0
//#define DEX_Initialize_Pin              LATAbits.LATA3//PORTAbits.RA3

#define DEX_RUN_Pin                     PORTAbits.RA0
#define DEX_Initialize_Pin              LATAbits.LATA3

#define LED_5                           LATCbits.LATC5     // BLE Disconnected
#define LED_6                           LATBbits.LATB4	  // BLE Connected
#define LED_7                           LATBbits.LATB5

#define TEST_LED LATBbits.LATB4

#define SnB                 0x01
#define TnC_BaseVersion     0x02
#define SANITORY            0x03
#define MMS                 0x04
#define PIZZA               0x05
#define OATS                0x06
#define ELEVEND             0x07


#define dHARDWARE_VERSION	0x01
#define dSOFWARE_VERSION	0x05
#define dMACHINE_TYPE       SnB
//------------------------------------------------------------------------------------------------------

typedef enum
{
	IDLE_STATE,
	BLUETOOTH_STATE,
	TRANSACTION_STATE,
	SERVICE_STATE,
    CARD_READER_STATE,

}eSystemState;

extern volatile eSystemState eNextState;
//extern volatile eSystemEvent eNewEvent;
int gucLastVendStatus;// __at(0x200);

extern unsigned char wrg_flg;
void pin_initilize(void);
//unsigned char Hex_to_char(unsigned char temp);

#endif	/* BASIC_REQUIREMENTS_H */

