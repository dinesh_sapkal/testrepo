/* 
 * File:   MDB_Protocol.h
 * Author: User
 *
 * Created on June 28, 2021, 12:23 PM
 */

#ifndef MDB_PROTOCOL_H
#define	MDB_PROTOCOL_H


#define VMC_ACK_A                   0x0000 // Acknowledgment, Mode-bit is Clr
#define VMC_ACK                     0x0100 // Acknowledgment, Mode-bit is set
#define VMC_NAK                     0x01FF // Negative Acknowledgment




#define VMC_RESET_M			0x110
#define VMC_RESET			0x010

#define VMC_SETUP			0x111

#define VMC_POLL_M			0x112
#define VMC_POLL			0x012

#define VMC_VEND			0x113
#define VMC_READER			0x114
#define VMC_REVALUE			0x115
#define VMC_EXPANSION		0x117







 //// SAiful
#define RES_RESET_M				1
#define RES_JUSTRESET			2
#define RES_CONFIGSETUP			3
#define RES_POLL_ACK			4
#define RES_PERI_ID				5
#define RES_READER				6
#define RES_VENDRQST			7
#define RES_VNDSUCCESS			8
#define RES_SESSIONCOMPLETE		9
#define RES_VEND_CANCEL			10
#define RES_VEND_FAIL_SLAVE		11
#define RES_CASH_VEND_SLAVE		12
#define RES_REVALUE_RQST		13
#define RES_SELECTION_DENIED		15


#define  ACK_RCV_JUST_RESET			1
#define  ACK_RCV_CONFIGDATA			2
#define  ACK_RCV_MAXMIN_PRICE		3
#define	 ACK_RCV_PERI_ID			4
#define	 ACK_RCV_START_SESSION		5
#define  ACK_RCV_VEND_APPROVE		6
#define  ACK_RCV_END_SESSION		7
#define  ACK_RCV_SESSION_CANCEL		8
#define  ACK_RCV_VEND_DENIED		9


#define TOAPP_VND_SUCCESS 1
#define TOAPP_VND_CANCEL  2
#define TOAPP_VND_FAIL    3




#define VMC_POLL_ACK           0x01
#define JUST_RESET              0x00
#define READER_CONFIG_INFO      0x01
#define DISPLAY_REQUEST         0x02
#define BEGIN_SESSION           0x03
#define SESSION_CANCEL_REQUEST  0x04
#define VEND_APPROVED           0x05
#define VEND_DENIED             0x06
#define END_SESSION             0x07
#define CANCELLED               0x08
#define PERIPHERAL_ID           0x09
#define MALFUNCTION_ERROR       0x0A
#define CMD_OUT_OF_SEQUENCE     0x0B
#define DIAGNOSTIC_RESPONSE     0xFF


// VMC_SETUP
#define VMC_CONFIG_DATA    0x00
#define VMC_MAX_MIN_PRICES 0x01

// VMC_READER
#define VMC_READER_DISABLE 0x00
#define VMC_READER_ENABLE  0x01
#define VMC_READER_CANCEL  0x02

// VMC_EXPANSION
#define VMC_EXPANSION_REQUEST_ID  0x00
#define VMC_EXPANSION_DIAGNOSTICS 0xFF

// Error name
#define VendCancelByVMC                     0x01
#define VendCancelByPriceMismatched         0x02
#define VendCancelWrongComprtment			0x03
#define VendCancelByTimeout                 0x04
#define KeyNotPressed                       0x05
#define KNoVendRqtForTap_N_Pay              0x06
#define EportVndStatusNotUpade              0x07
#define EportVCrediteExpired                0x08
#define KNoVendRqtForTap_N_Pay3             0x09
#define VendCancelByVMCTOEPORT				0X10
#define VendCancelWrongComprtmentEPORT		0x11
#define VendfailByTimeout                   0x12
#define No_creditFromEPORT					0x13
#define MDB_DisableAfter_BT_Connection		0x14

typedef struct
{
	unsigned int featureLevel;
	unsigned int countryCodeH;
	unsigned int countryCodeL;
	unsigned int scaleFactor;
	unsigned int decimalPlaces;
	unsigned int maxResponseTime; // seconds, overrides default NON-RESPONSE time
	unsigned int miscOptions;
} CASHLESS_DEVICE_Config_t;
 
 


extern CASHLESS_DEVICE_Config_t CASHLESS_Device_Config_INDIA,CASHLESS_Device_Config_US;

CASHLESS_DEVICE_Config_t CASHLESS_Device_Config_INDIA = {

	0x03, // featureLevel
	0x13,	//country code H
	0x56,	// country code L
	0x01, // Scale Factor
	0x02, // Decimal Places
	0x05, // Max Response Time
	//0x008  // Misc Options
//    	0x00D  // Misc Options
    0x009  // Misc Options
};

//// New Variable
unsigned int  guiMDB_RxData=0,guiLastMDB_RxData=0;
unsigned char  gucMDB_Step=0;


////****** New Variables
unsigned char ACK_Stage_State=0;
unsigned char MDB_Response_Stage=0;
unsigned char VMC_Configur_checksum=0;
unsigned char ucPeriID_checksum=0;
unsigned char ucResPollStage=0;
unsigned char ucSession_END_ACkRcv=0;
unsigned char ucRESET_RecvFLG=0;
unsigned char uc_CreditDoneFlg=0;
unsigned char Vend_statusToApp=0;
unsigned char ucSendVendResponseToApp=0;
unsigned int uiBLETransuctionCount=0;
unsigned char gucAmountCreditedPressKey=0;

unsigned char  ucPresentMDB_Stage=0;
unsigned char  uc_SudddenDisconnectFlg=0,E_PORT_CreditExpiredFlg=0,ucE_Port_CreditExpireStatustoApp;
unsigned char uc_VndReqstTimerFLG=0,ucVendingTimerExpiredFlg=0;
unsigned int ui_VndReqstTimerFCount=0;
unsigned char ucENDsessionCount=0;
unsigned int Eportcreditwaitcount=0;
unsigned char EportcreditwaitFlg=0,EportcreditwaitTimer_ExpiredFlg=0;

unsigned char Vend_Fail_ErrorName=0;

unsigned char gucVendCancelReasone;

unsigned int MDB_NEXT_POLL_STATE = JUST_RESET;

unsigned int gucMDB_RecBuff[80];
unsigned int guiMDB_RecBuff_Index=0;

unsigned char gucVMC_PER_ID[30];
unsigned char gucVMC_PER_ID_Index=0;

unsigned char ucInitialMDBCommError=0;

unsigned char gucMDB_ReaderEnable_Flag=0,gucMDB_ReaderCancel_Flag=0;
unsigned char gucStartRelayPressCounterFlag=0,gucRelayPressCounter=0;
unsigned char gucCancel_Session=0;
extern unsigned char gucAPPConnected,gucSendMachineReadyFlag,gucMachineReadySendCount,gucSendReadEnableFlag,gucReadEnableSendCount;
extern unsigned char gucReadenableWaitFlag,gucReadenableWaitCounter;

extern unsigned int guiReadEnableCounter;
extern unsigned char gucVendStartFlag;

extern unsigned char gucVMCRec_CMP_NUM[3];
extern unsigned int guiVMCReceivedCMP_Num;


extern unsigned char guiVendrequestTimerFlag,guiVendrequestTimerC0unter,gucPricMatchFlag;
extern unsigned int guiVMCReceivedPrice;



void MDB_Communication(void);

void Send_Cashless_Device_Configuration(void);
void Send_Acknowledgement(void);
void Send_Just_Reset(void);
void Approved_Vend(void);
void Denied_Vend(void);
void Cancel_Session(void);
void Vend_Cancelled(void);
void Start_Session(void);
void End_Session(void);
void Peripheral_ID_Send(void);
unsigned int calc_checksum(unsigned int *array, unsigned int arr_size);


#endif	/* MDB_PROTOCOL_H */

