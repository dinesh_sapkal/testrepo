/* 
 * File:   BLE_UART.h
 * Author: User
 *
 * Created on June 28, 2021, 12:22 PM
 */

#ifndef BLE_UART_H
#define	BLE_UART_H

/* initialize UART3 to transmit at 9600 Baud */
void initilize_BLE_uart(void);
void BLE_PutChar(unsigned char data);
void BLE_PutString(unsigned char *str);
void BLE_PutStringL(unsigned char *str,unsigned int len);

//unsigned char BLE_recieve_uart1(void);
//void BLE_CovertChar_in_ASCII(uint8_t data);

#endif	/* BLE_UART_H */

