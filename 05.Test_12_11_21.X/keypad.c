#include "basic_requirements.h"

#define motor_delay_ON()	__delay_ms(1000)
#define motor_delay_OFF()	__delay_ms(50)

unsigned char gucRx_Product_Code[5]={0,0,0,0,0};

const unsigned char Procudt_Code[72][6]={
	"001a", //0

	"0001", //1 A 0
	"0002", //2   2
	"0003", //3   3
	"0004", //4   4
	"0005", //5   5
	"0006", //6   6
	"0007", //7   7
	"0008", //8   8
	"0009", //9   9
	"0010",// 10  a9

	"0011", //1 B 11
	"0012", //2   12
	"0013", //3   13
	"0014", //4   14
	"0015", //5   15
	"0016", //6   16
	"0017", //7   17
	"0018", //8   18
	"0019", //9   19
	"0020", //10  20

	"0021", //1 C 21
	"0022", //2   22
	"0023", //3   23
	"0024", //4   24
	"0025", //5   25
	"0026", //6   26
	"0027", //7   27
	"0028", //8   28
	"0029", //9   29
	"0030", //10  30

	"0031", //1 D 31
	"0032", //2   32
	"0033", //3   33
	"0034", //4   34
	"0035", //5   35
	"0036", //6   36
	"0037", //7   37
	"0038", //8   38
	"0039", //9	  39
	"0040", //10  40

	"0041", //1 E 41
	"0042", //2   42
	"0043", //3   43
	"0044", //4   44
	"0045", //5   45
	"0046", //6   46
	"0047", //7   47
	"0048", //8   48
	"0049", //9   49
	"0050", //10  50

	"0051", //1 F 51
	"0052", //2   52
	"0053", //3   53
	"0054", //4   54
	"0055", //5   55
	"0056", //6   56
	"0057", //7   57
	"0058", //8   58
	"0059", //9   59
	"0060", //10  60

	"0061", //1 G 61
	"0062", //2   62
	"0063", //3   63
	"0064", //4   64
	"0065", //5   65
	"0066", //6   66
	"0067", //7   67
	"0068", //8   68
	"0069", //9   69
	"0070" //10  70
};


//const unsigned char Procudt_Code[72][6]={
//	"001a", //0
//
//	"0000", //1 A 0
//	"0001", //2   2
//	"0002", //3   3
//	"0003", //4   4
//	"0004", //5   5
//	"0005", //6   6
//	"0006", //7   7
//	"0007", //8   8
//	"0008", //9   9
//	"0009",// 10  a9
//
//	"0100", //1 B 11
//	"0101", //2   12
//	"0102", //3   13
//	"0103", //4   14
//	"0104", //5   15
//	"0105", //6   16
//	"0106", //7   17
//	"0107", //8   18
//	"0108", //9   19
//	"0109", //10  20
//
//	"0200", //1 C 21
//	"0201", //2   22
//	"0202", //3   23
//	"0203", //4   24
//	"0204", //5   25
//	"0205", //6   26
//	"0206", //7   27
//	"0207", //8   28
//	"0208", //9   29
//	"0209", //10  30
//
//	"0300", //1 D 31
//	"0301", //2   32
//	"0302", //3   33
//	"0303", //4   34
//	"0304", //5   35
//	"0305", //6   36
//	"0306", //7   37
//	"0307", //8   38
//	"0308", //9	  39
//	"0309", //10  40
//
//	"0400", //1 E 41
//	"0401", //2   42
//	"0402", //3   43
//	"0403", //4   44
//	"0404", //5   45
//	"0405", //6   46
//	"0406", //7   47
//	"0407", //8   48
//	"0408", //9   49
//	"0409", //10  50
//
//	"0500", //1 F 51
//	"0501", //2   52
//	"0502", //3   53
//	"0503", //4   54
//	"0504", //5   55
//	"0505", //6   56
//	"0506", //7   57
//	"0507", //8   58
//	"0508", //9   59
//	"0509", //10  60
//
//	"0600", //1 F 61
//	"0601", //2   62
//	"0602", //3   63
//	"0603", //4   64
//	"0604", //5   65
//	"0605", //6   66
//	"0606", //7   67
//	"0607", //8   68
//	"0608", //9   69
//	"0609" //10  70
//};

const unsigned char Payment_Code[36][8]={
"7A7058C",
"9A4098D",
"BA70A89",
"EAD0C81",
"0A4098D",
"DA9038F",
"EA60182",
"3A60888",
"7AE068D",
"7AF0B8F",
"5A70B8A",
"0AE068C",
"8A6088F",
"3AA0E82",
"1AA0988",
"BA7038C",
"FA70B8C",
"8A50E89",
"6AF0E8D",
"DAA0783",
"1A5078B",
"EA10589",
"AA9058D",
"5AD0E85",
"EAD0C8F",
"8A90F80",
"7A30D8A",
"0A00D88",
"5AD0783",
"EAF0B81",
"7AB0D8D",
"AA90986",
"3A80A80",
"DA00C87",
"FA70685",
"7AD0383"
};

const unsigned char Key_Matrix[7][4]={{'1','2','3'},
									  {'4','5','6'},
									  {'7','8','9'},
									  {'*','0','#'}};
									
//const unsigned char Key_Matrix[7][4]={  {'A','1','2'},
										//{'B','3','4'},
										//{'C','5','6'},
										//{'D','7','8'}};	


unsigned char ucMatched_Row=0, ucMatched_Col=0;
unsigned char ucKeyPressMask_Flg=0, ucKey_DelayTimerSRT_FLG=0,ucKeyEND_FLG=0;
unsigned int uiKey_DelayTimerCount=0;
volatile unsigned char ucKey_Sqnce_Number=0;


unsigned int compare_strings(const unsigned char a[], unsigned char b[])
{
	int c = 0;
	
	while (a[c] == b[c])
	{
		if (a[c] == '\0' || b[c] == '\0')
		break;
		c++;
	}
	
	if (a[c] == '\0' && b[c] == '\0')
		return 0;
	else
		return 1;
}

void motor_delay(void)
{
    unsigned int i;
    //for (i=0;i<5;i++)
    for (i=0;i<4;i++)
    {
        __delay_ms(50);
//        __delay_ms(50);
//        __delay_ms(50);
//        __delay_ms(50);
    }
}

void motor_Btn_delay(void)
{
   unsigned int i;
   for (i=0;i<3;i++)
	{
		//__delay_ms(50);
		//__delay_ms(50);
//		__delay_ms(50);
		__delay_ms(50);
	}
}


void Compare_2D_Array( unsigned char value)
{
    for (int row = 0; row < 7 ; row++)
    {
        for (int col = 0; col < 4; col++)
        {
            if(Key_Matrix[row][col]==value)
            {
                ucMatched_Row=row+1;
                ucMatched_Col=col+1;
                break;
            }
        }
    }

}



////**********************************************************************************************************************//
//********************************** void Pressed_Matrix_KeyON(unsigned char Row,unsigned char col)  *********************//
void Pressed_Matrix_KeyON(unsigned char Row,unsigned char col)
{
	switch (Row)
	{
		case 1:
				switch (col)
				{
					case 1: 
                            R1C1_RELAY=1;
//							BLE_PutString("\r\n R1C1");
							break;
					case 2: 
                            R1C2_RELAY=1;
//							BLE_PutString("\r\n R1C2");
							break;
					case 3: 
                            R1C3_RELAY=1;
//							BLE_PutString("\r\n R1C3");
							break;
					case 4: 
//                            R1C4_RELAY=1;
//							BLE_PutString("\r\n R1C42");
							break;
					default: break;
				}
				break;

		case 2:
				switch (col)
				{
					case 1: 
                            R2C1_RELAY=1;
//							BLE_PutString("\r\n R2C1");
							break;
					case 2: 
                            R2C2_RELAY=1;
//							BLE_PutString("\r\n R2C2");
							break;
					case 3: 
                            R2C3_RELAY=1;
//							BLE_PutString("\r\n R2C3");
							break;
					case 4: 
//                            R2C4_RELAY=1;
//							BLE_PutString("\r\n R2C4");
							break;
					default: break	;
				}
				break;

		case 3:
				switch (col)
				{
					case 1: 
                            R3C1_RELAY=1;
//							BLE_PutString("\r\n R3C1");
							break;
					case 2: 
                            R3C2_RELAY=1;
//							BLE_PutString("\r\n R3C2");
							break;
					case 3: 
                            R3C3_RELAY=1;
//							BLE_PutString("\r\n R3C3");
							break;
					case 4: 
//                            R3C4_RELAY=1;
//							BLE_PutString("\r\n R3C4");
							break;
					default: break;
				}
				break;

		case 4:
				switch (col)
				{
					case 1: 
//                            gpio_set_pin_level(R4C1_RELAY,1);
                            R4C1_RELAY=1;
//							BLE_PutString("\r\n R24C1");
							break;
					case 2: 
//                            gpio_set_pin_level(R4C2_RELAY,1);
                            R4C2_RELAY=1;
//							BLE_PutString("\r\n R4C2");
							break;
					case 3: 
//                            gpio_set_pin_level(R4C3_RELAY,1);
                            R4C3_RELAY=1;
//							BLE_PutString("\r\n R4C3");
							break;
					case 4: 
//                            gpio_set_pin_level(R4C4_RELAY,1);
//                            R4C4_RELAY=1;
//							BLE_PutString("\r\n R4C4");
							break;
					default: break;
				}
				break;
		default: break;
	}

}

////**********************************************************************************************************************//
//********************************** void Press_Keys(unsigned char Key_posi_Num)  ****************************************//
void Pressed_Matrix_KeyOFF(unsigned char Row1,unsigned char col1)
{
	switch (Row1)
	{
		case 1:
				switch (col1)
				{
					case 1: 
                            R1C1_RELAY=0;
//							BLE_PutString("\r\n OFR1C1");
							break;
					case 2: 
                            R1C2_RELAY=0;
//							BLE_PutString("\r\n OFR1C2");
							break;
					case 3: 
                            R1C3_RELAY=0;
//							BLE_PutString("\r\n OFR1C3");
							break;
					case 4: 
//                            R1C4_RELAY=0;
//							BLE_PutString("\r\n OFR1C42");
							break;
					default: break;
				}
				break;

		case 2:
				switch (col1)
				{
					case 1: 
                            R2C1_RELAY=0;
//							BLE_PutString("\r\n OFR2C1");
							break;
					case 2: 
                            R2C2_RELAY=0;
//							BLE_PutString("\r\n OFR2C2");
							break;
					case 3: 
                            R2C3_RELAY=0;
//							BLE_PutString("\r\n OFR2C3");
							break;
					case 4: 
//                            R2C4_RELAY=0;
//							BLE_PutString("\r\n OFR2C4");
							break;
					default: break	;
				}
				break;

		case 3:
				switch (col1)
				{
					case 1:     
                            R3C1_RELAY=0;
//							BLE_PutString("\r\n OFR3C1");
							break;
					case 2: 
                            R3C2_RELAY=0;
//							BLE_PutString("\r\n OFR3C2");
							break;
					case 3: 
                            R3C3_RELAY=0;
//							BLE_PutString("\r\n OFR3C3");
							break;
					case 4: 
//                            R3C4_RELAY=0;
//							BLE_PutString("\r\n OFR3C4");
							break;
					default: break;
				}
				break;

		case 4:
				switch (col1)
				{
					case 1: 
//                            gpio_set_pin_level(R4C1_RELAY,0);
                            R4C1_RELAY=0;
//							BLE_PutString("\r\n OFR24C1");
							break;
					case 2: 
//                            gpio_set_pin_level(R4C2_RELAY,0);
                            R4C2_RELAY=0;
//							BLE_PutString("\r\n OFR4C2");
							break;
					case 3: 
//                            gpio_set_pin_level(R4C3_RELAY,0);
                            R4C3_RELAY=0;
//							BLE_PutString("\r\n OFR4C3");
							break;
					case 4: 
//                            gpio_set_pin_level(R4C4_RELAY,0);
//							R4C4_RELAY=0;
//                            BLE_PutString("\r\n OFR4C4");
							break;
					default: break;
				}
				break;
		default: break;
	}

}





////**********************************************************************************************************************//
void Start_Key_Pressing(void)
{
	
	gucAmountCreditedPressKey=1;
	ucKeyPressMask_Flg=1;
	ucKey_Sqnce_Number=1;
    
    uiKey_DelayTimerCount=0;
    ucKey_DelayTimerSRT_FLG=0;
}
//********************************** void Clear_Key_Pressing(void)  *****************************************************//
void Clear_Key_Pressing(void)
{
	
	gucAmountCreditedPressKey=0;
	ucKeyPressMask_Flg=0;
	ucKey_Sqnce_Number=0;
	ucKey_DelayTimerSRT_FLG=0;
	uiKey_DelayTimerCount=0;
}
//********************************** void Press_Keys(unsigned char Key_posi_Num)  ****************************************//
void Press_Keys(unsigned char Key_posi_Num)
{
	if (ucKeyPressMask_Flg==1)
	{
		switch (Key_posi_Num)
		{
			case 1:
					if (gucRx_Product_Code[0]>=0x31)
					{
//						BLE_PutString("\r\nProduct Code: ");
//                        BLE_PutChar(gucRx_Product_Code[0]);
						Compare_2D_Array(gucRx_Product_Code[0]);
                        Pressed_Matrix_KeyON(ucMatched_Row,ucMatched_Col);
						ucKey_DelayTimerSRT_FLG=1;
						uiKey_DelayTimerCount=0;
						ucKeyPressMask_Flg=0;
						//BLE_PutString("\r\n Key case 11");
//						BLE_PutString("\r\nKey Pos 1");
					}
					else
					{
						ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
						ucKeyPressMask_Flg=1;
//						BLE_PutString("\r\nKey Pos 1 is NULL");
					}
					//BLE_PutString("\r\n Key case 1");
					break;
			case 2:
					if (gucRx_Product_Code[1]>=0x31)
					{
//						BLE_PutString("\r\nProduct Code: ");
//                        BLE_PutChar(gucRx_Product_Code[1]);
						Compare_2D_Array(gucRx_Product_Code[1]);
                        Pressed_Matrix_KeyON(ucMatched_Row,ucMatched_Col);
						ucKey_DelayTimerSRT_FLG=1;
						uiKey_DelayTimerCount=0;
						ucKeyPressMask_Flg=0;
						//BLE_PutString("\r\n Key case 22");
//						BLE_PutString("\r\nKey Pos 2");
					}
					else
					{
						ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
						ucKeyPressMask_Flg=1;
//						BLE_PutString("\r\nKey Pos 2 is NULL");
					}
					//BLE_PutString("\r\n Key case 2");
					break;

			case 3:
					if (gucRx_Product_Code[2]>=0x30)
					{
//						BLE_PutString("\r\nProduct Code: ");
//                        BLE_PutChar(gucRx_Product_Code[2]);
						Compare_2D_Array(gucRx_Product_Code[2]);
                        Pressed_Matrix_KeyON(ucMatched_Row,ucMatched_Col);
						ucKey_DelayTimerSRT_FLG=1;
						uiKey_DelayTimerCount=0;
						ucKeyPressMask_Flg=0;
						//BLE_PutString("\r\n Key case 33");
//						BLE_PutString("\r\nKey Pos 3");
					}
					else
					{
						ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
						ucKeyPressMask_Flg=1;
//						BLE_PutString("\r\nKey Pos 3 is NULL");
					}
					//BLE_PutString("\r\n Key case 3");
					break;
			case 4:
					if (gucRx_Product_Code[3]>=0x30)
					{
//                        BLE_PutString("\r\nProduct Code: ");
//                        BLE_PutChar(gucRx_Product_Code[3]);
						Compare_2D_Array(gucRx_Product_Code[3]);
						Pressed_Matrix_KeyON(ucMatched_Row,ucMatched_Col);
						ucKey_DelayTimerSRT_FLG=1;
						uiKey_DelayTimerCount=0;
						ucKeyPressMask_Flg=0;
						//ucKeyEND_FLG=1;
//						BLE_PutString("\r\nKey Pos 4");
				
					}
					else
					{
						//ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
						ucKeyPressMask_Flg=0;
						ucKeyEND_FLG=1;
//						BLE_PutString("\r\nKey Pos 4 is NULL");
				
					}
					//BLE_PutString("\r\n Key case 4");
					break;
			
			default: //ucKeyPressMask_Flg=1;
					 BLE_PutString("\r\n Key Not Matched");
//					 MDB_logInfo.MDB_Logbyte1=Set_Bit(MDB_logInfo.MDB_Logbyte1,6);  // MDB_Logbyte1 Bit 6
					break;
			
		}
	}
	
	else if (ucKeyPressMask_Flg==2)
	{
		
		switch (Key_posi_Num)
		{
			case 1:
					Pressed_Matrix_KeyOFF(ucMatched_Row,ucMatched_Col);
					ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
					ucKeyPressMask_Flg=0;
					
					//BLE_PutString("\r\ncase 1 key off");
					//Debug_PutChar(ucKey_Sqnce_Number+0x30);
					break;
			case 2:
					Pressed_Matrix_KeyOFF(ucMatched_Row,ucMatched_Col);
					ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
					ucKeyPressMask_Flg=0;
					
					//BLE_PutString("\r\ncase 2 key off");
					//Debug_PutChar(ucKey_Sqnce_Number+0x30);
					break;

			case 3:
					Pressed_Matrix_KeyOFF(ucMatched_Row,ucMatched_Col);
					ucKey_Sqnce_Number=ucKey_Sqnce_Number+1;
					ucKeyPressMask_Flg=0;
					
					//BLE_PutString("\r\ncase 3 key off");
					//Debug_PutChar(ucKey_Sqnce_Number+0x30);
					break;
			case 4:
					Pressed_Matrix_KeyOFF(ucMatched_Row,ucMatched_Col);
					ucKeyPressMask_Flg=0;
					ucKey_DelayTimerSRT_FLG=0;
					uiKey_DelayTimerCount=0;
					ucKeyPressMask_Flg=0;
					ucKeyEND_FLG=1;
					ucKey_Sqnce_Number=0;
					//BLE_PutString("\r\ncase 4 key off");
					//Debug_PutChar(ucKey_Sqnce_Number+0x30);
					break;
			
			default: //ucKeyPressMask_Flg=1;
					break;
			
		}
	}
	
}


void PressMachineKey(unsigned char gucMCMotorNumber)
{	
	switch(gucMCMotorNumber)
	{
		case 1: //01
		
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				////__delay_ms(50);__delay_ms(20);

				break;

		case 2: //02
			
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				
				break;

		case 3: //03
				
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				
				break;
	
		case 4: //04
				
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				
				break;

		case 5: //05
				
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				
				break;

		case 6: //06
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				
				break;

		case 7: //07
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_7_TCN=0x01;
				motor_delay();
				KEY_7_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
			
				break;

		case 8: //08
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_8_TCN=0x01;
				motor_delay();
				KEY_8_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				
				break;

		case 9: //09
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_9_TCN=0x01;
				motor_delay();
				KEY_9_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 10: //10
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 11: //11
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 12: //12
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				
				motor_delay();
					
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 13: //13
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 14://14
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
	
		case 15://15
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 16: //16
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 17: //17
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_7_TCN=0x01;
				motor_delay();
				KEY_7_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 18: //18
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_8_TCN=0x01;
				motor_delay();
				KEY_8_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
		case 19: //19
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_9_TCN=0x01;
				motor_delay();
				KEY_9_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 20: //20 
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				
				break;
		

		case 21: //21
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);

				break;

		case 22: //22
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
	
		case 23: //23
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 24: //24
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 25: //25
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
		

		case 26: //26
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				
				break;

		case 27: //27
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_7_TCN=0x01;
				motor_delay();
				KEY_7_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 28: //28
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_8_TCN=0x01;
				motor_delay();
				KEY_8_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
	
		case 29: //29
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_9_TCN=0x01;
				motor_delay();
				KEY_9_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 30: //30 
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 31: //31

				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 32: //32
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 33://33
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 34: //34
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 35: //35
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
	
		case 36: //36
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 37: //37
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_7_TCN=0x01;
				motor_delay();
				KEY_7_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 38: //38
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_8_TCN=0x01;
				motor_delay();
				KEY_8_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 39: //39
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_9_TCN=0x01;
				motor_delay();
				KEY_9_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
		
		case 40: // 40
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
		
		case 41: //41
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
	
		case 42: //42
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 43: //43
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 44: //44
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 45: //45
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;


		case 46: //E6
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
	
		case 47: //47
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_7_TCN=0x01;
				motor_delay();
				KEY_7_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 48: //48
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_8_TCN=0x01;
				motor_delay();
				KEY_8_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 49: //49
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_9_TCN=0x01;
				motor_delay();
				KEY_9_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 50: //50
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 51: //51
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
	
		case 52: //52
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 53: //53
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 54: //54
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 55: //55
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;


		case 56: //56
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
	
		case 57: //57
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_7_TCN=0x01;
				motor_delay();
				KEY_7_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 58: //58
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_8_TCN=0x01;
				motor_delay();
				KEY_8_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
		case 59: //F9
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_9_TCN=0x01;
				motor_delay();
				KEY_9_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;

		case 60: // 60
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
		case 61: // 61
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_1_TCN=0x01;
				motor_delay();
				KEY_1_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;				
		case 62: // 62
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_2_TCN=0x01;
				motor_delay();
				KEY_2_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
		case 63: // 63
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_3_TCN=0x01;
				motor_delay();
				KEY_3_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;				
		case 64: // 64
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_4_TCN=0x01;
				motor_delay();
				KEY_4_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
		case 65: // 65
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_5_TCN=0x01;
				motor_delay();
				KEY_5_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
		case 66: // 66
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
		case 67: // 67
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_7_TCN=0x01;
				motor_delay();
				KEY_7_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
		case 68: // 68
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_8_TCN=0x01;
				motor_delay();
				KEY_8_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
//		case 68: // 68
//				KEY_6_TCN=0x01;
//				motor_delay();
//				KEY_6_TCN=0x00;
//				
//				motor_Btn_delay();
//					
//				KEY_8_TCN=0x01;
//				motor_delay();
//				KEY_8_TCN=0x00;
//				//__delay_ms(50);__delay_ms(20);
//				break;
		case 69: // 69
				KEY_6_TCN=0x01;
				motor_delay();
				KEY_6_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_9_TCN=0x01;
				motor_delay();
				KEY_9_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
		case 70: // 70
				KEY_7_TCN=0x01;
				motor_delay();
				KEY_7_TCN=0x00;
				
				motor_Btn_delay();
					
				KEY_0_TCN=0x01;
				motor_delay();
				KEY_0_TCN=0x00;
				//__delay_ms(50);__delay_ms(20);
				break;
																																						
		default :	break;

	}
}


unsigned char Hex_to_char(unsigned char temp)
{
	if(temp==0x00)
	{
		return(temp+0x30);
	}
	else if(temp>=0x01 && temp<=0x09)
	{
		return(temp+0x30);
	}
	else if(temp==0x0A)
	{
		return('A');
	}
	else if(temp==0x0B)
	{
		return('B');
	}
	else if(temp==0x0C)
	{
		return('C');
	}
	else if(temp==0x0D)
	{
		return('D');
	}
	else if(temp==0x0E)
	{
		return('E');
	}
	else if(temp==0x0F)
	{
		return('F');
	}
	else
	{
		return(0xFF);
	}
}
