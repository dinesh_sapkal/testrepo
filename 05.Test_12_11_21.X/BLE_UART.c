#include "basic_requirements.h"


/* initialize UART3 to transmit at 9600 Baud */
void initilize_BLE_uart(void) 
{	
	TRISD6 = 0;   					// TX Pin
	TRISD7 = 1;   					// RX Pin
    
    // disable interrupts before changing states
    PIE3bits.RC2IE = 0;
    
    // ABDOVF no_overflow; CKTXP async_noninverted_sync_fallingedge; BRG16 16bit_generator; WUE disabled; ABDEN disabled; DTRXP not_inverted; 
    BAUDCON2 = 0x08;

    // SPEN enabled; RX9 8-bit; CREN enabled; ADDEN disabled; SREN disabled; 
    RCSTA2 = 0x90;

    // TX9 8-bit; TX9D 0; SENDB sync_break_complete; TXEN enabled; SYNC asynchronous; BRGH hi_speed; CSRC slave_mode; 
    TXSTA2 = 0x24;

    // 
    SPBRG2 = 0x17;

    // 
    SPBRGH2 = 0x00;

    // enable receive interrupt
    PIE3bits.RC2IE = 1;
}

void BLE_PutChar(unsigned char data) 
{
    PIR3bits.TX2IF=0;
    while(0 == PIR3bits.TX2IF)
    {
    }

    TXREG2 = data;    // Write the data byte to the USART.
}

void BLE_PutString(unsigned char *str)
{
	unsigned int i=0;
    while(str[i]!='\0')                 //check for NULL character to terminate loop
    {
//        PIR3bits.TX2IF=0;
//        while(0 == PIR3bits.TX2IF);
//        TXREG2 = str[i];                     // Load data onto buffer
        BLE_PutChar(str[i]);
        i++;
    }
}


void BLE_PutStringL(unsigned char *str,unsigned int len)
{
	unsigned int i=0;
    while(len>0)
    {
//        PIR3bits.TX2IF=0;
//        while(0 == PIR3bits.TX2IF)
//        {
//        }
//        TXREG2 = str[i];        
        BLE_PutChar(str[i]);
        len--;
        i++; 
         __delay_ms(5);
    }
}

/* read a character from UART3 */
//unsigned char BLE_recieve_uart1(void) 
//{
//	char c=0;
//
////	while(!(REG_SERCOM5_USART_INTFLAG & 4)) {}  /* wait until receive buffer is full */
////	c = REG_SERCOM5_USART_DATA;                 /* read the receive char */
//	return c;
//}
//
//void BLE_CovertChar_in_ASCII(uint8_t data)
//{
//	unsigned char tempData,tempHex,tempHex1;
//	
//    tempData = data;
//	tempHex=((tempData & 0xF0) >>4);
//	tempHex1=((tempData & 0x0F));
//	
//    BLE_PutChar(Hex_to_char(tempHex));
//	BLE_PutChar(Hex_to_char(tempHex1));
//	BLE_PutChar(' ');
//}