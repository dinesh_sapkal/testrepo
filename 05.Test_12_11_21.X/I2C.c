
#include "basic_requirements.h"

#define     MAXPAGEWRITE   64



 /*********************************************************************************
  Function   :  i2c_init
  Description:  This function is used to initialize I2C Communication 
  Parameters :  NONE
  Returns    :  NONE
  Remarks    :
 **********************************************************************************/
void i2c_init(void)
{	
	TRISCbits.TRISC4 = 1;		// Make SDA and 
	TRISCbits.TRISC3 = 1;		// SCK pins input

	SSP1ADD  = ((_XTAL_FREQ/4000)/100) - 1;	
  	SSP1STAT = 0x80;     // Slew Rate is disable for 100 kHz mode
  	SSP1CON1 = 0x28;     // Enable SDA and SCL, I2C Master mode, clock = FOSC/(4 * (SSPADD + 1))
  	SSP1CON2 = 0x00;     // Reset MSSP Control Register
  	PIR1bits.SSPIF=0;   // Clear MSSP Interrupt Flag
}


 /*********************************************************************************
  Function   :  i2c_idle
  Description:  This function is used to check line is IDLE 
  Parameters :  NONE
  Returns    :  NONE
  Remarks    :
 **********************************************************************************/
void i2c_idle(void)
{
  // Wait I2C Bus and Status Idle (i.e. ACKEN, RCEN, PEN, RSEN, SEN)
	  while (( SSPCON2 & 0x1F ) || ( SSPSTATbits.R_NOT_W));
}


 /*********************************************************************************
  Function   :  i2c_start
  Description:  This function is used to start the I2C sequence
  Parameters :  NONE
  Returns    :  NONE
  Remarks    :
 **********************************************************************************/
void i2c_start(unsigned char stype)
{
	i2c_idle();                     // Ensure the I2C module is idle
  	if (stype == I2C_START) 
	{
    	SSPCON2bits.SEN = 1;          // Start I2C Transmission
    	while(SSPCON2bits.SEN);
  	}
	else
	{
    	SSPCON2bits.RSEN = 1;         // ReStart I2C Transmission
    	while(SSPCON2bits.RSEN);
  	}
}

 /*********************************************************************************
  Function   :  i2c_stop
  Description:  This function is used to stop the I2C sequence
  Parameters :  NONE
  Returns    :  NONE
  Remarks    :
 **********************************************************************************/
void i2c_stop(void)
{
	SSPCON2bits.PEN = 1;	  // Stop I2C Transmission
  	while(SSPCON2bits.PEN);
}



 /*********************************************************************************
  Function   :  i2c_write
  Description:  This function is used to write data on I2C
  Parameters :  NONE
  Returns    :  NONE
  Remarks    :
 **********************************************************************************/
void i2c_write(unsigned char data)
{
   	SSPBUF = data;				  // Send the Data to I2C Bus
  	if (SSPCON1bits.WCOL)         // Check for write collision
    	return;  
  	while(SSPSTATbits.BF);        // Wait until write cycle is complete
  	i2c_idle();                   // Ensure the I2C module is idle
}

 /*********************************************************************************
  Function   :  i2c_read
  Description:  This function is used to read data on I2C
  Parameters :  NONE
  Returns    :  NONE
  Remarks    :
 **********************************************************************************/
unsigned char i2c_read(void)
{
	i2c_idle();      // Ensure the I2C module is idle            
  	// Enable Receive Mode
  	SSPCON2bits.RCEN = 1;           // Enable master for 1 byte reception
  	while(!SSPSTATbits.BF);         // Wait until buffer is full
  	return(SSPBUF);
}

 /*********************************************************************************
  Function   :  i2c_master_ack
  Description:  This function is used to check acknowledgement 
  Parameters :  NONE
  Returns    :  NONE
  Remarks    :
 **********************************************************************************/
void i2c_master_ack(unsigned char ack_type)
{
	SSPCON2bits.ACKDT = ack_type;   // 1 = Not Acknowledge, 0 = Acknowledge
  	SSPCON2bits.ACKEN = 1;          // Enable Acknowledge
  	while (SSPCON2bits.ACKEN == 1);
}

 /*********************************************************************************
  Function   :  Read_EEPROM
  Description:  This function is used to read data from EEPROM
  Parameters :  Memory Address
  Returns    :  unsigned char data
  Remarks    :
 **********************************************************************************/
unsigned char Read_EEPROM(unsigned int mem_addr)
{
	unsigned char data;
  	i2c_start(I2C_START);  // Start the I2C Transmission
  	i2c_write(ADDR|I2C_WRITE);    // Write Control Byte - Write
  	i2c_write(mem_addr>>8);      // Sending the 8-Bit Memory Address Pointer
    i2c_write(mem_addr&0x00ff);      // Sending the 8-Bit Memory Address Pointer
  	i2c_start(I2C_REP_START);  // ReStart the I2C Transmission
  	i2c_write(ADDR|I2C_READ);   // Write Control Byte - Read
  	data=i2c_read();   // Read Data from EEPROM
  	i2c_master_ack(I2C_DATA_NACK);    // Master send No Acknowledge Required to the Slave
  	i2c_stop();    // Stop the I2C Transmission
  	return(data);
}

 /*********************************************************************************
  Function   :  Write_EEPROM
  Description:  This function is used to write data from EEPROM
  Parameters :  Memory Address, Data
  Returns    :  NONE
  Remarks    :
 **********************************************************************************/
void Write_EEPROM(unsigned int mem_addr,unsigned char data)
{
  	i2c_start(I2C_START);  // Start the I2C Write Transmission
  	i2c_write(ADDR|I2C_WRITE);     // Write I2C OP Code
  	i2c_write(mem_addr>>8);      // Sending the 8-Bit Memory Address Pointer
    i2c_write(mem_addr&0x00ff);      // Sending the 8-Bit Memory Address Pointer
  	i2c_write(data);     // Write data to  EEPROM
  	i2c_stop();  	 // Stop I2C Transmission
  	__delay_ms(8);  // Put some delay upto 5ms here
}

// /*********************************************************************************
//  Function   :  EEPROM_PageWrite
//  Description:  This function is used to write data in EEPROM by page wise
//  Parameters :  Memory Address, Data,length of data
//  Returns    :  NONE
//  Remarks    :
// **********************************************************************************/
//void EEPROM_PageWrite(unsigned int mem_addr, unsigned char *data, unsigned int tLen) 
//{
//    unsigned int n,i;
//    unsigned int maxBytes = 64-(mem_addr%64);  //find the difference bet age length & address  
//    unsigned int buff_index = 0;
//    
//   
//    while (tLen>0)
//    {
//        if(tLen<=maxBytes) 
//        {
//            n=tLen ;     
//        }
//        else
//        {
//            n=maxBytes;
//        }
//        i2c_start(I2C_START);  // Start the I2C Write Transmission
//        i2c_write(ADDR|I2C_WRITE);     // Write I2C OP Code
//        i2c_write(mem_addr>>8);      // Sending the 8-Bit Memory Address Pointer
//        i2c_write(mem_addr&0x00ff);      // Sending the 8-Bit Memory Address Pointer
//        
//        for (i = 0; i < n; i++)
//            i2c_write(data[buff_index + i]);
//        
//        i2c_stop();  	 // Stop I2C Transmission
//    
//        mem_addr=mem_addr+n;  
//        buff_index=buff_index+n;
//        tLen=tLen-n;
//        maxBytes = 64; 
//        __delay_ms(8);  // Put some delay upto 5ms here
//    }
//}

