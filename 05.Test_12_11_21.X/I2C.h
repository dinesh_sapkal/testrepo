/* 
 * File:   I2C.h
 * Author: User
 *
 * Created on June 28, 2021, 12:34 PM
 */

#ifndef I2C_H
#define	I2C_H


#define I2C_DATA_ACK 0
#define I2C_DATA_NACK 1
#define I2C_WRITE 0
#define I2C_READ 1

#define I2C_START 0
#define I2C_REP_START 1
#define I2C_REQ_ACK 0
#define I2C_REQ_NOACK 0

#define ADDR  0xA0 //opcode of I2C EEPROM


void i2c_init(void);
void i2c_idle(void);
void i2c_start(unsigned char stype);
void i2c_stop(void);
void i2c_write(unsigned char data);
unsigned char i2c_read(void);
void i2c_master_ack(unsigned char ack_type);
unsigned char Read_EEPROM(unsigned int mem_addr);
void Write_EEPROM(unsigned int mem_addr,unsigned char data);
unsigned long int read(unsigned int add,unsigned char bytes);
void write(unsigned int add,unsigned char bytes,unsigned char *arr);

//void EEPROM_PageWrite(unsigned int mem_addr, unsigned char *data, unsigned int tLen) ;


#endif	/* I2C_H */

