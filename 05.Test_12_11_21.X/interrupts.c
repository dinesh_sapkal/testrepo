#include "basic_requirements.h"


unsigned char en_rx_buf=0,bluetooth_step=0,Dollar_Rcv_Flg=0;

void __interrupt() INTERRUPT_InterruptManager()
{
    unsigned int tempRXData_1;
    unsigned char bit9th,tempRXData_2,MDBChecksum;
    volatile unsigned char bleRXData=0;
    
    
    //UART1 MDB interrupt
    if(PIE1bits.RC1IE == 1 && PIR1bits.RC1IF == 1) 
    {
        if(1 == RCSTA1bits.OERR)
        {
            // EUSART2 error - restart
            RCSTA1bits.CREN = 0;
            RCSTA1bits.CREN = 1;

        }

        // buffer overruns are ignored
        bit9th = RX9D1;
        tempRXData_1 = bit9th;	
        tempRXData_1 = tempRXData_1<<8;
        guiMDB_RxData =  tempRXData_1 | RCREG1;
        
        switch (gucMDB_Step)
		{
			case 0 :
						switch (guiMDB_RxData)
						{
							case VMC_ACK_A		:
													switch (ACK_Stage_State)
													{
														case ACK_RCV_JUST_RESET:
                                                                                    MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                    ACK_Stage_State=0;
                                                                                    break;
														case ACK_RCV_CONFIGDATA:
                                                                                    ACK_Stage_State=0;
                                                                                    MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                    break;
														case ACK_RCV_PERI_ID:
                                                                                    ACK_Stage_State=0;
                                                                                    MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                    break;
														case ACK_RCV_START_SESSION:
//                                                                                    gucStartRelayPressCounterFlag=1;
//                                                                                    gucRelayPressCounter=0;
                                                                                    BLE_PutString("start ssn \r\n");
//                                                                                    if (ucVendProcessRunning)
                                                                                    {
                                                                                      Start_Key_Pressing();
                                                                                    }
                                                                                    
                                                                                    ACK_Stage_State=0;
                                                                                    MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                    ucResPollStage=VMC_POLL_ACK;
                                                                                    gucMDB_Step=0;
                                                                                    break;
														case ACK_RCV_VEND_APPROVE:
//                                                                                    BLE_PutString("vnd approve ack \r\n");
                                                                                    ACK_Stage_State=0;
                                                                                    MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                    ucResPollStage=VMC_POLL_ACK;
                                                                                    break;
														case ACK_RCV_END_SESSION:
//                                                                                    BLE_PutString("end ssn ack \r\n");
                                                                                    ACK_Stage_State=0;
                                                                                    ucPresentMDB_Stage=0;
                                                                                    MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                    ucResPollStage=VMC_POLL_ACK;
                                                                                    ucSession_END_ACkRcv=1;  // For  Vend Status send to APP
                                                                                    break;
														case ACK_RCV_SESSION_CANCEL:
//                                                                                    BLE_PutString("ssn cancel ack \r\n");
                                                                                    ACK_Stage_State=0;
                                                                                    gucMDB_Step=0;
                                                                                    MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                    ucResPollStage=VMC_POLL_ACK;
                                                                                    break;
														case ACK_RCV_VEND_DENIED:
//                                                                                    BLE_PutString("vnd denied ack \r\n");
                                                                                    ACK_Stage_State=0;
                                                                                    MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                    ucResPollStage=VMC_POLL_ACK;
                                                                                    gucMDB_Step=0;
                                                                                    break;
														default                 :			
                                                                                    break;
													}
													break;
							case VMC_RESET_M    :

													break;
							case VMC_RESET		:
													if (guiLastMDB_RxData==VMC_RESET_M)
													{
														MDB_Response_Stage=RES_RESET_M;
//														BLE_PutString("\r\n VMC_45");
														ucRESET_RecvFLG=1;
													}
													break;
							case VMC_SETUP     :
													guiMDB_RecBuff_Index=0;
													gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_SETUP;
													gucMDB_Step=1;
													break;
							case VMC_POLL_M    :
													break;
							case VMC_POLL      :
                                                    if (ucRESET_RecvFLG)
                                                    {
                                                        switch(MDB_NEXT_POLL_STATE)
                                                        {
                                                            case VMC_POLL_ACK  :   
                                                                                    MDB_Response_Stage=RES_POLL_ACK;
                                                                                    break;

                                                            case JUST_RESET             :
                                                                                            //Debug_PutString("\r\n VMC_123");
                                                                                            MDB_Response_Stage=RES_JUSTRESET;
                                                                                            break;
                                                            default						:
                                                                                            break;
                                                        }

                                                    }
                                                    else
                                                    {
                                                        //
                                                        if (ucInitialMDBCommError)
                                                        {
                                                            MDB_PutChar(0x000);
                                                            MDB_PutChar(VMC_ACK);
                                                            BLE_PutString("\r\nPOlll");
                                                        }
                                                    }
                                                    break;
							case VMC_VEND      :
                                                    guiMDB_RecBuff_Index=0;
                                                    gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_VEND;
                                                    gucMDB_Step=4;
                                                    break;
							case VMC_READER    :
                                                    guiMDB_RecBuff_Index=0;
                                                    gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_READER;
                                                    gucMDB_Step=3;
                                                    break;
							case VMC_REVALUE    :  
                                                    guiMDB_RecBuff_Index=0;
                                                    gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_REVALUE;
                                                    gucMDB_Step=12;
                                                    break;
							case VMC_EXPANSION :
                                                    guiMDB_RecBuff_Index=0;
                                                    gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_EXPANSION;
                                                    gucMDB_Step=2;
                                                    break;												
							case VMC_NAK        :
                                                    //BLE_PutString("\r\nMDB_NAK");                  
                                                    break;  
							default			   :
                                                    break;
						}
						break;
			case 1:
						//collecting the VMC SETP & VMC PRICE Data
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=7)
						{
							MDB_Response_Stage=RES_CONFIGSETUP;
						}
						break;
			case 2:
						//collecting the VMC EXPANSION data
                
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                        
//						if(guiMDB_RecBuff_Index>31)
//						{
//                         
//                            MDB_Response_Stage=RES_PERI_ID;
//						}
                        switch(gucMDB_RecBuff[1])
                        {
                            case 0x00:
                                        if(guiMDB_RecBuff_Index>31)
                                        {

//                                            BLE_PutChar('A');
                                            MDB_Response_Stage=RES_PERI_ID;
                                        }
                                        break;
                            case 0x04:
                                        if(guiMDB_RecBuff_Index>7)
                                        {
//                                            BLE_PutChar('B');
                                            MDB_Response_Stage=RES_PERI_ID;
                                        }
                                        break;
                            default:
                                break;
                        }
						break;
			case 3:
						//Collecting the VMC READER data
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=3)
						{
//							MDB_Response_Stage=RES_READER;
                            
                            unsigned char checksum = calc_checksum(gucMDB_RecBuff, 2);
							checksum &= 0x00FF;

                            if (checksum != gucMDB_RecBuff[2])
                            {
                                MDB_PutChar(VMC_NAK);
                            }
							else
							{
//								BLE_PutString("\r\nVMC Reader Checksum Matched");
								switch(gucMDB_RecBuff[1])
								{
									case VMC_READER_DISABLE :
																Send_Acknowledgement();
																BLE_PutString("read disable\r\n");
																MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																ucResPollStage=VMC_POLL_ACK;
																gucMDB_ReaderEnable_Flag=0;
                                                                gucMDB_ReaderCancel_Flag=0;
                                                                ucRESET_RecvFLG=0;
                                                                LED_5=1; // LED OFF
                                                                MDB_Response_Stage=0;
                                                                gucMDB_Step=0;
                                                                break;
									case VMC_READER_ENABLE  :
                                                            
                                                                MDB_Response_Stage=RES_READER;
                                                                Send_Acknowledgement();
																BLE_PutString("read enable\r\n");
                                                                
																MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																ucResPollStage=VMC_POLL_ACK;
																gucMDB_ReaderEnable_Flag=1;
                                                                gucMDB_ReaderCancel_Flag=0;
                                                                LED_5=0; // LED ON
																break;
									case VMC_READER_CANCEL  :
                                                               MDB_Response_Stage=RES_READER;
																Vend_Cancelled();
																BLE_PutString("READER CANCEL\r\n");
																gucMDB_ReaderEnable_Flag=0;
																gucMDB_ReaderCancel_Flag=1;
                                                                LED_5=1; // LED OFF
                                                                ucRESET_RecvFLG=0;
																break;
									default : break;
								}
							}
							MDB_Response_Stage=0;
							gucMDB_Step=0;
						}
						break;
			case 4:
						//Vend Request Data
						switch(guiMDB_RxData)
						{
							case 0x00	:
                                            //vend request
                                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                            gucMDB_Step=5;
                                            break;
							case 0x01	:
                                            //vend cancel
                                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                            gucMDB_Step=6;
                                            break;
							case  0x02	:
                                            //vend success
                                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                            gucMDB_Step=7;
                                            break;
							case 0x03	:
                                            //vend fail
                                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                            gucMDB_Step=8;
                                            break;
							case 0x04	:
                                            //ssn complete
                                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                            gucMDB_Step=9;
                                            break;
							case 0x05	:
                                            //Cash Vend
                                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                            gucMDB_Step=10;
                                            break;
                            case 0x07	:
                                            //  Selection denied
                                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                            gucMDB_Step=15;
                                            break;
							default     :
                                            break;
						}
						break;
			case 5:
						//Vend Request by VMC
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=7)
						{
							MDB_Response_Stage=RES_VENDRQST;
						}
						break;
			case 6:
						//vend cancel
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=3)
						{
							MDB_Response_Stage=RES_VEND_CANCEL;
						}
						break;
			case 7:
						//vend success
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=5)
						{
							MDB_Response_Stage=RES_VNDSUCCESS;
						}
						break;
			case 8:
						//vend fail
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=3)
						{
							MDB_Response_Stage=RES_VEND_FAIL_SLAVE;
						}
						break;


			case 9:
						//ssn complete
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if((guiMDB_RecBuff_Index>=2))
						{
							MDB_Response_Stage=RES_SESSIONCOMPLETE;
                        }
						break;
			case 10:
						//vend using coin
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=7)
						{
							MDB_Response_Stage=RES_CASH_VEND_SLAVE;
						}
						break;
			case 11:
						gucVMC_PER_ID[gucVMC_PER_ID_Index++]=guiMDB_RxData;
						if (gucVMC_PER_ID_Index>25)
						{
							gucVMC_PER_ID_Index=0;
							gucMDB_Step=0;
						}
						break;
			case 12: 	
						switch(guiMDB_RxData)
						{
							case 0x00	:
											//Revalue request
											gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
											gucMDB_Step=13;
											break;
							case 0x01	:
											//Revalue Limit
											gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
											guiMDB_RecBuff_Index=0;
											gucMDB_Step=0;
											break;
						}
						break;

			case 13:  	
						gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
						if(guiMDB_RecBuff_Index>=7)
						{
							MDB_Response_Stage=RES_REVALUE_RQST;
						}
						break;	
            case 15:     // Selection denied
                        gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                        if((guiMDB_RecBuff_Index>=6))
                        {

                            MDB_Response_Stage=RES_SELECTION_DENIED;
                        }

                        break;
			default:
                        break;
		}

		guiLastMDB_RxData = guiMDB_RxData;
        
        PIR1bits.RC1IF=0;
    }
    
    if(PIE3bits.RC2IE == 1 && PIR3bits.RC2IF == 1) // Blue-tooth UART ISR
    {
        if(1 == RCSTA2bits.OERR)
        {
            // EUSART2 error - restart

            RCSTA2bits.CREN = 0;
            RCSTA2bits.CREN = 1;
        }

        // buffer overruns are ignored
        bleRXData = RCREG2;
        
        //SendByteSerially_BT(dt);
        if(wrg_flg==1)
        { 
            en_rx_buf=0;
            wrg_flg = 0; 
            Dollar_Rcv_Flg=0;
            guiStar_buff_index=0;
            guiDollor_buff_index=0;
        }
        
        if(bleRXData =='*')
        {
            en_rx_buf++;
            bluetooth_step=2;
            //SendStringSerially_BT_Debug("\r\nVRcv*");
        }
        else if(bleRXData=='$')
        {
            Dollar_Rcv_Flg++;
            bluetooth_step=3;
        }		

        switch(bluetooth_step)
        {
            case 2:
//                        if(en_rx_buf==1)
//                        {
//                            gucStar_buffer[guiStar_buff_index++] = bleRXData ;	
//                        }
//                        else if(en_rx_buf==2)
//                        {
//                            gucStar_buffer[guiStar_buff_index++] = bleRXData ;
//                            en_rx_buf=0;
//                            bleRXData=0;	
//                        }
//
//                        if (guiStar_buff_index >= (MaxChars-1))//
//                        {
//
//                            gucStarStringRCV_Flag=1;
//                            guiStar_buff_index = 0;
//                        }
//                        
                        if((bleRXData == '*') && (guiStar_buff_index==0))
                        {
                //            guiBLE_RXBuffer_Index=0;
                            gucStar_buffer[guiStar_buff_index]=bleRXData;		//save the data after start request Header
                            guiStar_buff_index++;   
                        }
                        else if((bleRXData != '*') && (guiStar_buff_index>0))
                        {
                            gucStar_buffer[guiStar_buff_index]=bleRXData;		//save the data after start request Header
                            guiStar_buff_index++;   
                        }
                         else if((bleRXData == '*') && (guiStar_buff_index>0))
                        {
                            gucStar_buffer[guiStar_buff_index]=bleRXData;		//save the data after start request Header
                            guiStar_buff_index++;   
                            gucStarStringRCV_Flag=1;
                             en_rx_buf=0;
                        }
                        break;
            case 3:
                        if  (Dollar_Rcv_Flg==1)
                        {
                            gucDollor_buffer[guiDollor_buff_index++]=bleRXData; 
//                            bleRXData=0;
//                            BLE_PutString("1stDol");
                        }
                        else if(Dollar_Rcv_Flg==2)
                        {
                            gucDollor_buffer[guiDollor_buff_index++]=bleRXData;
                            gucDollor_buffer[guiDollor_buff_index]='\0';
                            Dollar_Rcv_Flg=0;
                            gucDollerStringRCV_Flag=1;
                            guiDollor_buff_index=0;
//                            BLE_PutString("2ndDol");
//                            bleRXData=0;	
                        }
                        break;  
        }
        
        PIR3bits.RC2IF=0;
    }
    if(INTCONbits.TMR0IE == 1 && INTCONbits.TMR0IF == 1)
    {
        // TMR0H 120; 
        TMR0H = 0x78;

        // TMR0L 255; 
        TMR0L = 0xFF;
    
        
        //----- wait to send read enable ----------        
        if(gucReadenableWaitFlag==1)
        {
            gucReadenableWaitCounter++;
            if(gucReadenableWaitCounter>50)
            {
                gucSendReadEnableFlag=1;
                gucReadenableWaitFlag=0;
                gucReadenableWaitCounter=0;
            }
        }
        //---------------------------------------------
        
       //// ****************  This timer  used for  pressing the keys 
	
        if (ucKey_DelayTimerSRT_FLG)
        {
            if ((ucKeyPressMask_Flg==1)||(ucKeyPressMask_Flg==0))
            {
                uiKey_DelayTimerCount++;
            }
            //if (uiKey_DelayTimerCount==8)//for TCN vend stop
            //if (uiKey_DelayTimerCount==5)
            if (uiKey_DelayTimerCount==8)//on time
            {
//                BLE_PutString("\r\nA1");
                ucKeyPressMask_Flg=2;
            }
//            else if(uiKey_DelayTimerCount>=16)
//            else if(uiKey_DelayTimerCount>=10)
            else if(uiKey_DelayTimerCount>=16)//OFF time
            {
//                BLE_PutString("\r\nA2");
                ucKey_DelayTimerSRT_FLG=0;
                uiKey_DelayTimerCount=0;
                ucKeyPressMask_Flg=1;

            }
        }
	
//// *******************************************	

        if (uc_VndReqstTimerFLG)
        {
            ui_VndReqstTimerFCount++;

//            BLE_PutString("\r\nEPORTCredit TTTTTT:");
            if (ui_VndReqstTimerFCount>SEC*45)
            {
//                BLE_PutString("Vending Timeout:\r\n");  
                
                if(ucSession_END_ACkRcv==0)
                {
                    gucCancel_Session=1;
                    ucVendingTimerExpiredFlg=1;
                    uc_VndReqstTimerFLG=0;
                    ui_VndReqstTimerFCount=0;
                }
                else
                {
                    BLE_PutString("T_Flag\r\n");  
                    ucVendingTimerExpiredFlg=1;
                    uc_VndReqstTimerFLG=0;
                    ui_VndReqstTimerFCount=0;
                }
            }

        }

        if(gucStartRelayPressCounterFlag)
        {
            gucRelayPressCounter++;

             if((gucRelayPressCounter>=SEC*20))
             {
                 gucRelayPressCounter=0;
                 gucStartRelayPressCounterFlag=0;
                 gucCancel_Session=1;
             }
        }

        if(guiVendrequestTimerFlag==1)
        {
            guiVendrequestTimerC0unter++;
            if(guiVendrequestTimerC0unter>15)
            {
                guiVendrequestTimerC0unter=0;
                guiVendrequestTimerFlag=0;
                if((guiVMCReceivedPrice==guiBLERecPrice))//&&(!uc_SudddenDisconnectFlg))
                {
                    gucPricMatchFlag = 1;
//                    MDB_Response_Stage = RES_POLL_ACK;
                    ucResPollStage = VEND_APPROVED;
                    gucRevert_String[7]= '0';
                    BLE_PutString("price matched\r\n");
                }
                else
                {

                    gucVendCancelReasone=VendCancelByPriceMismatched;
                    //ucResPollStage = VEND_DENIED;
//                    MDB_Response_Stage= RES_POLL_ACK;
                    ucResPollStage = SESSION_CANCEL_REQUEST;
                    gucPricMatchFlag = 0;
                    gucRevert_String[7]= '1';
                    BLE_PutString("price not matched\r\n");
                    guiBLERecPrice=0;
                }   
                MDB_Response_Stage=0;
            }
        }
        
        // clear the TMR0 interrupt flag
        INTCONbits.TMR0IF = 0;
    }
    
}
