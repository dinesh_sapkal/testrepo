#include "basic_requirements.h"


unsigned char gucPricMatchFlag=0,gucVendStartFlag=0;
unsigned char gucVMCRec_CMP_NUM[3];
unsigned int guiVMCReceivedCMP_Num=0;

unsigned char gucWaitCounter=0;
unsigned char guiVendrequestTimerFlag=0,guiVendrequestTimerC0unter=0;
unsigned int guiVMCReceivedPrice=0;

void MDB_Communication(void)
{
    unsigned int checksum=0;
    
    
    if (ucSendVendResponseToApp)
    {
        gucRevert_String[3] = gucStar_buffer[21];
        gucRevert_String[4] = gucStar_buffer[22];
        gucRevert_String[5] = gucStar_buffer[23];
                                
        switch (Vend_statusToApp)
        {
            case TOAPP_VND_SUCCESS:
                                    LiveCashDataUpdate();
                                    
                                     //BLE_PutString("\r\nSuccess Send To APP");
                                     Vend_statusToApp=0;
                                     ui_VndReqstTimerFCount=0;
                                     uc_VndReqstTimerFLG=0;
                                     
                                     //save the sales compartment in the gucRevert_string Buffer
                                     gucRevert_String[0]='D';
                                     gucRevert_String[7]='0';//
                                    BLE_PutStringL(gucRevert_String,9);
                                    BLE_PutChar('1');
                                    BLE_PutString("\r\n");

                                    BLE_PutString("sucess\r\n");
                                    __delay_ms(500);
                                    BLE_PutString("sucess\r\n");
                                    __delay_ms(500);
                                    BLE_PutString("sucess\r\n");
                                    __delay_ms(500);
                                    BLE_PutString("ssn complete\r\n");
                                    __delay_ms(500);
                                    BLE_PutString("end ssn\r\n");

                                    break;
          case TOAPP_VND_CANCEL:
                                     //BLE_PutString("\r\nCancel Send To APP");
                                     Vend_statusToApp=0;
                                     ui_VndReqstTimerFCount=0;
                                     uc_VndReqstTimerFLG=0;
                                    
                                     BLE_PutString("vend cancelled\r\n");

                                     gucRevert_String[0]='D';
                                    if(gucPricMatchFlag)
                                    {
                                        gucRevert_String[7]= '1';
                                    }
                                    else
                                    {
                                        gucRevert_String[7]= '6';
                                    }

//                                    Write_EEPROM(1,0); //eeprom_write(add,LastVendStatus);
                                    BLE_PutStringL(gucRevert_String,9);
                                    BLE_PutChar('0');
                                    BLE_PutString("\r\n");



                                    __delay_ms(50);			                   // added this becuse APP not catching no vend case 23_aug_2019
                                    BLE_PutString("no vend case\r\n");	// added for because vnd denide not handled in app 23_aug_2019
                                    __delay_ms(500);
                                    BLE_PutString("no vend case\r\n");
                                    __delay_ms(500);
                                    BLE_PutString("no vend case\r\n");
                                    __delay_ms(500);
                                    BLE_PutString("ssn complete\r\n");
                                    __delay_ms(500);
                                    BLE_PutString("end ssn\r\n");
                                     break;
          case TOAPP_VND_FAIL:
                                     //BLE_PutString("\r\nVend Fail  To APP");
                                     Vend_statusToApp=0;
                                     ui_VndReqstTimerFCount=0;
                                     uc_VndReqstTimerFLG=0;
                                     
                                     BLE_PutString("vnd fail\r\n");
                                     
                                    gucRevert_String[0]='D';
                                    gucRevert_String[7]= '5';	
                                    BLE_PutStringL(gucRevert_String,9);
                                    BLE_PutChar('0');
                                    BLE_PutString("\r\n");	
                                    __delay_ms(50);
                                    BLE_PutString("no vend case\r\n");
                                    __delay_ms(500);
                                    BLE_PutString("no vend case\r\n");
                                    __delay_ms(500);
                                    BLE_PutString("no vend case\r\n");
                                     __delay_ms(500);
                                    BLE_PutString("ssn complete\r\n");
                                    __delay_ms(500);
                                    BLE_PutString("end ssn\r\n");
                                    break;
            default: 

                                     break;
        }  
        
        for(int xc=0;xc<=30000;xc++);
        
        ucSendVendResponseToApp=0;
        //BLE_PutString("\r\nClearing bit0 of Logbyte4");
         gucVendStartFlag=0;
         gucReadenableWaitFlag=1;
         gucReadenableWaitCounter=0;
//         gucSendReadEnableFlag=1;
         gucReadEnableSendCount=0;
         guiReadEnableCounter=20000;
         
        gucRelayPressCounter=0;
        gucCancel_Session=0;
        gucStartRelayPressCounterFlag=0;
        
//         __delay_ms(500);	
        
        
    }
    switch (MDB_Response_Stage)
	{
		case RES_RESET_M:
                                Send_Acknowledgement();
                                __delay_ms(10);
                                Send_Just_Reset();
                                MDB_NEXT_POLL_STATE=JUST_RESET;
                                MDB_Response_Stage=0;
                                //BLE_PutString("\r\nMDB_Response_Stage1");
                                BLE_PutString("Reset\r\n");
                                break;
		case RES_JUSTRESET:
                                //BLE_PutString("\r\nMDB_Response_Stage2");
                                Send_Just_Reset();
                                ACK_Stage_State=ACK_RCV_JUST_RESET;
                                MDB_Response_Stage=0;
                                gucMDB_Step=0;
                                BLE_PutString("Just Reset\r\n");
                                break;
		case RES_CONFIGSETUP:
                                VMC_Configur_checksum = calc_checksum(gucMDB_RecBuff, 6);
                                VMC_Configur_checksum &= 0x00FF;

                                // compare calculated and received checksums
                                if (VMC_Configur_checksum != gucMDB_RecBuff[6])
                                {
                                    MDB_PutChar(VMC_NAK);
                                }
                                else
                                {
                                    //BLE_PutString("\r\nMDB_Response_Stage3");
                                    switch(gucMDB_RecBuff[1])
                                    {
                                        case VMC_CONFIG_DATA :
                                                                    BLE_PutString("Got MDB VMC Configuration Data\r\n");
                                                                    Send_Cashless_Device_Configuration(); //sending the cashless device configuration to VMC                                                                    
                                                                    ACK_Stage_State=ACK_RCV_CONFIGDATA;
                                                                    break;
                                        case VMC_MAX_MIN_PRICES :
                                                                    BLE_PutString("Got MDB VMC MAX-MIN Price Data\r\n");
                                                                    Send_Acknowledgement();
//                                                                    BLE_PutString("setprice\r\n");
                                                                    MDB_NEXT_POLL_STATE = VMC_POLL_ACK;
                                                                    break;
                                        default :
                                                                    break;
                                    }
                                }
                                MDB_Response_Stage=0;
                                gucMDB_Step=0;
                                break;
	case RES_POLL_ACK   :
                                switch (ucResPollStage)
                                {

									case VMC_POLL_ACK           :   
                                                                    Send_Acknowledgement();
                                                                    if (ucSession_END_ACkRcv)
                                                                    {
                                                                        gucWaitCounter++;
                                                                        if(gucWaitCounter>5)
                                                                        {
                                                                            ucSession_END_ACkRcv=0;
                                                                            ucSendVendResponseToApp=1;
                                                                        }
                                                                    }
																	MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
																	break;

									case BEGIN_SESSION          :
																	//BLE_PutString("\r\nSession Start New ");
																	ucPresentMDB_Stage=1;
																	
																	Start_Session();
                                                                    ACK_Stage_State=ACK_RCV_START_SESSION;
																	ucENDsessionCount=0;
																	break;
									case SESSION_CANCEL_REQUEST :
                                                                    //BLE_PutString("\r\nCancel_session ");
                                                                    
                                                                    Cancel_Session();
                                                                    ACK_Stage_State= ACK_RCV_SESSION_CANCEL;
                                                                    gucMDB_Step=0;
                                                                    Vend_statusToApp=TOAPP_VND_CANCEL;
                                                                    break;
									case VEND_APPROVED          :
                                                                    BLE_PutString("vend approved\r\n");
                                                                    ucPresentMDB_Stage=2;
                                                                    
                                                                    Approved_Vend();
                                                                    ACK_Stage_State= ACK_RCV_VEND_APPROVE;
                                                                    break;
									case VEND_DENIED            :
                                                                    BLE_PutString("vnd denied\r\n");
                                                                    ucPresentMDB_Stage=2;
                                                                    
                                                                    Vend_statusToApp=TOAPP_VND_CANCEL;
                                                                    Denied_Vend();
                                                                    ACK_Stage_State= ACK_RCV_VEND_DENIED;
																	break;
									case END_SESSION           :
                                                                    ucENDsessionCount++;
                                                                    if (ucENDsessionCount<10)
                                                                    {
                                                                        
                                                                        ucPresentMDB_Stage=3;
                                                                        End_Session();
                                                                        ucSession_END_ACkRcv=1;  // For  Vend Status send to APP
                                                                        ACK_Stage_State=ACK_RCV_END_SESSION;
                                                                    }
                                                                    else
                                                                    {

                                                                        MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                        ucResPollStage=VMC_POLL_ACK;

                                                                    }
                                                                    break;
									case CANCELLED              ://VMC Reader cancel Stage
                                                                    MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                    ucResPollStage=VMC_POLL_ACK;
																	break;
									default						:

																	break;
                                }
                                MDB_Response_Stage=0;
                                gucMDB_Step=0;
                                break;
	case RES_PERI_ID        :
//                                ucPeriID_checksum = calc_checksum(gucMDB_RecBuff, 31);
//                                ucPeriID_checksum &= 0x00FF;
//                                
//                                if (ucPeriID_checksum != gucMDB_RecBuff[31])
//                                {
//                                    BLE_PutChar('c');
//                                    MDB_PutChar(VMC_NAK);
//                                }
//                                else
//                                {
//                                    switch(gucMDB_RecBuff[1])
//                                    {
//                                        case VMC_EXPANSION_REQUEST_ID  :
//                                            BLE_PutChar('d');
//                                                                        Peripheral_ID_Send();
//                                                                        ACK_Stage_State=ACK_RCV_PERI_ID;
////                                                                        MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
//                                                                        ucResPollStage=VMC_POLL_ACK;
//                                                                        break;
//                                        case 0x04:
//                                            BLE_PutChar('e');
//                                                    Send_Acknowledgement();
//																BLE_PutString("got expansion2\r\n");
//																MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
//																ucResPollStage=VMC_POLL_ACK;
//                                            break;
//                                        default :
//                                                         break;
//                                    }
//                                }
                                
                                if(gucMDB_RecBuff[1]==0x00)
									{
										checksum = calc_checksum(gucMDB_RecBuff, 31);
										checksum &= 0x00FF;
										// compare calculated and received checksums
										if (checksum != gucMDB_RecBuff[31])
										{
											//BLE_PutString("Wrong Expansion Checksum \r\n");
											MDB_PutChar(VMC_NAK);
										}
										else
										{
											//BLE_PutString("Expansion Checksum Matched\r\n");
											switch(gucMDB_RecBuff[1])
											{
												case VMC_EXPANSION_REQUEST_ID  :
                                                                                BLE_PutString("Got Expansion ID\r\n");
                                                                                Peripheral_ID_Send();
                                                                                ACK_Stage_State=ACK_RCV_PERI_ID;
                                                                                ucResPollStage=VMC_POLL_ACK;
                                                                                break;
												default : break;
											}
										}
									}
									else if(gucMDB_RecBuff[1]==0x04)
									{
										checksum = calc_checksum(gucMDB_RecBuff, 6);
										checksum &= 0x00FF;
										// compare calculated and received checksums
										if (checksum != gucMDB_RecBuff[6])
										{
											//BLE_PutString("Wrong Expansion Checksum \r\n");
											MDB_PutChar(VMC_NAK);
										}
										else
										{
											BLE_PutString("Got Expansion2\r\n");
											Send_Acknowledgement();
											MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                            ucResPollStage=VMC_POLL_ACK;
										}
									}
                                MDB_Response_Stage=0;
                                gucMDB_Step=0;
                                break;
	case RES_READER     :     
//							checksum = calc_checksum(gucMDB_RecBuff, 2);
//							checksum &= 0x00FF;
//
//                            if (checksum != gucMDB_RecBuff[2])
//                            {
//                                MDB_PutChar(VMC_NAK);
//                            }
//							else
//							{
////								BLE_PutString("\r\nVMC Reader Checksum Matched");
//								switch(gucMDB_RecBuff[1])
//								{
////									case VMC_READER_DISABLE :
////																Send_Acknowledgement();
////																BLE_PutString("read disable\r\n");
////																MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
////																ucResPollStage=VMC_POLL_ACK;
////																gucMDB_ReaderEnable_Flag=0;
////                                                                gucMDB_ReaderCancel_Flag=0;
////                                                                LED_5=1; // LED OFF
////                                                                break;
//									case VMC_READER_ENABLE  :
////                                                                Send_Acknowledgement();
//																//BLE_PutString("read enable\r\n");//comment beacaus its coming continuosly
//																if(gucDEX_DoneFlag)
//                                                                {
//                                                                   gucDEX_DoneFlag=0;
//                                                                   
//                                                                   gucSendReadEnableFlag=1;
//                                                                    gucReadEnableSendCount=1;
//                                                                    guiReadEnableCounter=20000;
//                                                                }
////                                                                if(uc_VndReqstTimerFLG==0)//if no vending state then only change the state
//                                                                {
//                                                                    MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
//                                                                    ucResPollStage=VMC_POLL_ACK;
//                                                                }
//																gucMDB_ReaderEnable_Flag=1;
//                                                                gucMDB_ReaderCancel_Flag=0;
//                                                                LED_5=0; // LED ON
//																break;
//									case VMC_READER_CANCEL  :
//																Vend_Cancelled();
//																BLE_PutString("READER CANCEL\r\n");
//																gucMDB_ReaderEnable_Flag=0;
//																gucMDB_ReaderCancel_Flag=1;
//                                                                LED_5=1; // LED OFF
//																break;
//									default : break;
//								}
//							}
//							MDB_Response_Stage=0;
//							gucMDB_Step=0;
							break;

	case RES_VENDRQST   :
//                            BLE_PutString("\r\nVend 1Request from VMC:- ");
                            checksum = calc_checksum(gucMDB_RecBuff,6);
                            checksum &= 0x00FF;

                            if (checksum != gucMDB_RecBuff[6])
                            {
                                MDB_PutChar(VMC_NAK);
                                //MDB_logInfo.MDB_Logbyte1=Set_Bit(MDB_logInfo.MDB_Logbyte1,7);  // MDB_Logbyte1 Bit 7
                            }
                            else
                            {

                                Send_Acknowledgement();

                                gucRelayPressCounter=0;
                                gucCancel_Session=0;
                                gucStartRelayPressCounterFlag=0;

//                                hardwareInfo.last_vend_status = 0x00;//last vend status as no Status before transaction
                                guiVMCReceivedPrice = gucMDB_RecBuff[2];
                                guiVMCReceivedPrice = (guiVMCReceivedPrice<<8)+gucMDB_RecBuff[3];
                                
                                //--------------------Find Compartment Number-----------------------
                                    gucVMCRec_CMP_NUM[0] = ((gucMDB_RecBuff[4])%10);
                                    gucVMCRec_CMP_NUM[1] = ((gucMDB_RecBuff[5])/10);
                                    gucVMCRec_CMP_NUM[2] = ((gucMDB_RecBuff[5])%10);
                                //--------------------Find Compartment Number-----------------------
                                
//                                 BLE_PutString("VMC Price:- ");
//                                BLE_PutChar((guiVMCReceivedPrice/1000)+0x30);
//                                BLE_PutChar(((guiVMCReceivedPrice/100)%10)+0x30);
//                                BLE_PutChar(((guiVMCReceivedPrice/10)%10)+0x30);
//                                BLE_PutChar((guiVMCReceivedPrice%10)+0x30);
//                                BLE_PutString("\r\n");
                                
                                gucRevert_String[1]=((guiVMCReceivedPrice/100)/256)+0x30;
                                gucRevert_String[2]=((guiVMCReceivedPrice/100)%256)+0x30;
                                
                                
                                // for the TCN VendStop  Machine
                                guiVMCReceivedCMP_Num = gucMDB_RecBuff[4];
                                guiVMCReceivedCMP_Num = (guiVMCReceivedCMP_Num<<8)+gucMDB_RecBuff[5];
                                
//                                BLE_PutString("\r\nVMC Compartment:- ");
//                                BLE_PutChar((guc_CMP_TempSale_Count_Before_Inc_Len/1000)+0x30);
//                                BLE_PutChar(((guc_CMP_TempSale_Count_Before_Inc_Len/100)%10)+0x30);
//                                BLE_PutChar(((guiVMCReceivedCMP_Num/10))+0x30);
//                                BLE_PutChar((guiVMCReceivedCMP_Num%10)+0x30);
                                
                                if(guiVMCReceivedCMP_Num<10)
                                {
                                    gucVMCRec_CMP_NUM[0] = guiVMCReceivedCMP_Num;
//                                    BLE_PutChar(gucVMCRec_CMP_NUM[0]+0x30);
                                }
                                else if((guiVMCReceivedCMP_Num>=10) && (guiVMCReceivedCMP_Num<=99))
                                {
                                    gucVMCRec_CMP_NUM[0] = (guiVMCReceivedCMP_Num/10);
                                    gucVMCRec_CMP_NUM[1] = (guiVMCReceivedCMP_Num%10);
////                                  
//                                     BLE_PutChar(gucVMCRec_CMP_NUM[0]+0x30);
//                                     BLE_PutChar(gucVMCRec_CMP_NUM[1]+0x30);
                                }
                                

                               BLE_PutString("vend request\r\n");
                               guiVendrequestTimerFlag=0;
                                if((guiVMCReceivedPrice==guiBLERecPrice))//&&(!uc_SudddenDisconnectFlg))
                                {
                                    gucPricMatchFlag = 1;
                                    ucResPollStage = VEND_APPROVED;
                                    gucRevert_String[7]= '0';
                                    BLE_PutString("price matched\r\n");
                                }
                                else
                                {
                                    
                                    gucVendCancelReasone=VendCancelByPriceMismatched;
                                    //ucResPollStage = VEND_DENIED;
                                    ucResPollStage = SESSION_CANCEL_REQUEST;
                                    gucPricMatchFlag = 0;
                                    gucRevert_String[7]= '1';
                                    BLE_PutString("price not matched\r\n");
                                    guiBLERecPrice=0;
                                }   
                            }
                            Calculate_Compartment_Number();
                            MDB_Response_Stage=0;
                            gucMDB_Step=0;
                            break;

	case  RES_VNDSUCCESS :
                            checksum = calc_checksum(gucMDB_RecBuff,4);
                            checksum &= 0x00FF;

                            if (checksum != gucMDB_RecBuff[4])
                            {
                                MDB_PutChar(VMC_NAK);
                            }
                            else
                            {
                                //BLE_PutString("\r\n Vend Success from VMC:- ");

                                MDB_PutChar(VMC_ACK);
                                Vend_statusToApp=TOAPP_VND_SUCCESS;
                           }
                           ucResPollStage = VMC_POLL_ACK;
                           gucMDB_Step=0;
                           MDB_Response_Stage=0;
                           break;

	case RES_SESSIONCOMPLETE:
                            checksum = calc_checksum(gucMDB_RecBuff,2);
							checksum &= 0x00FF;

                            if (checksum != gucMDB_RecBuff[2])
                            {
                                MDB_PutChar(VMC_NAK);
                            }
                            else
                            {
                                Send_Acknowledgement();
//                                BLE_PutString("\r\nSession Complete 4m VMC(VNK)");
                                MDB_Response_Stage=RES_POLL_ACK;
                                ucResPollStage = END_SESSION;
                            }
                            gucMDB_Step=0;
                            MDB_Response_Stage=0;
                            break;
	case RES_VEND_CANCEL:
							checksum = calc_checksum(gucMDB_RecBuff,2);
							checksum &= 0x00FF;

							if (checksum != gucMDB_RecBuff[2])
							{
								MDB_PutChar(VMC_NAK);
							}
							else
							{
								{
                                    Send_Acknowledgement();
                                    gucVendCancelReasone=VendCancelByVMC;
                                    Vend_statusToApp=TOAPP_VND_CANCEL;
                                    BLE_PutString("vmc vnd cancel\r\n");
                                    
                                    ucKeyEND_FLG=0;
                                    Clear_Key_Pressing();
                                    gucRelayPressCounter=0;
                                    gucCancel_Session=0;
                                    gucStartRelayPressCounterFlag=1;
								}
							}

							MDB_Response_Stage=RES_POLL_ACK;
							ucResPollStage = VEND_DENIED;
                            gucMDB_Step=0;
                            break;
	case  RES_VEND_FAIL_SLAVE:
							checksum = calc_checksum(gucMDB_RecBuff,2);
							checksum &= 0x00FF;

							if (checksum != gucMDB_RecBuff[2])
							{
								MDB_PutChar(VMC_NAK);
							}
							else
							{
                                Send_Acknowledgement();
//                                    BLE_PutString("\r\nVend Fail from VMC (VNK)");
                                MDB_Response_Stage=RES_POLL_ACK;
                                Vend_statusToApp=TOAPP_VND_FAIL;
                                Vend_Fail_ErrorName=0x01 ;
							}
							gucMDB_Step=0;
							break; 
	case RES_CASH_VEND_SLAVE:
                            checksum = calc_checksum(gucMDB_RecBuff,6);
                            checksum &= 0x00FF;

                            if (checksum != gucMDB_RecBuff[6])
                            {
                                MDB_PutChar(VMC_NAK);
                            }
                            else
                            {
                                MDB_PutChar(VMC_ACK);
//                                Vend_statusToApp=TOAPP_VND_SUCCESS;
//                                hardwareInfo.last_vend_status = 0x01;//last vend status as success
//                                BLE_PutString("\r\nVend Success 4m VMC(VNK) Pr_0");
                                
                                //--------------------Find Compartment Number-----------------------
                                    gucVMCRec_CMP_NUM[0] = ((gucMDB_RecBuff[4])%10);
                                    gucVMCRec_CMP_NUM[1] = ((gucMDB_RecBuff[5])/10);
                                    gucVMCRec_CMP_NUM[2] = ((gucMDB_RecBuff[5])%10);
                                //--------------------Find Compartment Number-----------------------
                                
                                Calculate_Compartment_Number();
                                LiveCashDataUpdate();
                                
                                ucResPollStage=VMC_POLL_ACK;
                            }
                            gucMDB_Step=0;
                            MDB_Response_Stage=0;
                            guiMDB_RecBuff_Index=0;
//                            BLE_PutString("\r\nCashVend");
                            break;

	case RES_REVALUE_RQST:
							checksum = calc_checksum(gucMDB_RecBuff,6);
							checksum &= 0x00FF;
							if (checksum != gucMDB_RecBuff[6])
							{
									MDB_PutChar(VMC_NAK);
                            }
							else
							{
                                Send_Acknowledgement();
                            }
                            gucMDB_Step=0;
                            MDB_Response_Stage=0;
                            guiMDB_RecBuff_Index=0;
//                            BLE_PutString("\r\nCRevalue Reqst");
                            break;
        case RES_SELECTION_DENIED:  
                                 
                                 BLE_PutString("Selection Denied\r\n");
								 checksum = calc_checksum(gucMDB_RecBuff,5);
                                 checksum &= 0x00FF;
								if (checksum != gucMDB_RecBuff[5])
								{
									MDB_PutChar(VMC_NAK);
								}
								else
								{
                                            Send_Acknowledgement();
											Vend_statusToApp=TOAPP_VND_CANCEL;
                                            gucRevert_String[6]= 	'0';
                                            gucRevert_String[7]= 	'7';
                                            gucCancel_Session=1;
                                }
                                gucMDB_Step=0;
                                MDB_Response_Stage=0;
                                guiMDB_RecBuff_Index=0;
                                break;
		default         : 
                            break;
	}
    
    if(gucAmountCreditedPressKey)
    {        
        gucAmountCreditedPressKey=0;

        for(int i=0;i<=61;i++)
        {
            char b=0;
            b=compare_strings(&Procudt_Code[i][0],gucRx_Product_Code);
            if (b==0)
            {
                PressMachineKey(i);
                guiRelayPressCounter=0;
                gucStartRelayPressCounterFlag=1;
                break;
            }
            else
            {
//                BLE_PutString("\r\n Product Code not Match");
            }
        }
    }
    
    else if(gucCancel_Session)
    {
        gucCancel_Session=0;
        BLE_PutString("Canceling...\r\n");
        ucResPollStage=SESSION_CANCEL_REQUEST;
        gucVendCancelReasone=VendCancelByTimeout;
        gucMDB_Step=0;
    }
    
    if (ucVendingTimerExpiredFlg)
    {
        ucVendingTimerExpiredFlg=0;
        //BLE_PutString("VNK Timer Expired\r\n");
        ucSendVendResponseToApp=1;
        Vend_statusToApp=TOAPP_VND_FAIL;
    }
}



////*****************************************************************//
void Send_Cashless_Device_Configuration(void)
{
	unsigned int checksum=0;
	// calculate checksum, no Mode bit yet
	checksum = ( READER_CONFIG_INFO + CASHLESS_Device_Config_INDIA.featureLevel
                                    + CASHLESS_Device_Config_INDIA.countryCodeH
                                    + CASHLESS_Device_Config_INDIA.countryCodeL
                                    + CASHLESS_Device_Config_INDIA.scaleFactor
                                    + CASHLESS_Device_Config_INDIA.decimalPlaces
                                    + CASHLESS_Device_Config_INDIA.maxResponseTime
                                    + CASHLESS_Device_Config_INDIA.miscOptions );

	//__delay_ms(1);	__delay_us(800);
	MDB_PutChar(READER_CONFIG_INFO);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(CASHLESS_Device_Config_INDIA.featureLevel);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(CASHLESS_Device_Config_INDIA.countryCodeH);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(CASHLESS_Device_Config_INDIA.countryCodeL);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(CASHLESS_Device_Config_INDIA.scaleFactor);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(CASHLESS_Device_Config_INDIA.decimalPlaces);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(CASHLESS_Device_Config_INDIA.maxResponseTime);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(CASHLESS_Device_Config_INDIA.miscOptions);
    checksum=(checksum |0x0100);
   
	MDB_PutChar(checksum);

	BLE_PutString("Send the MDB Slave Setup Configuration\r\n");
}


void Send_Acknowledgement(void)
{
	__delay_ms(1);	__delay_us(800);
	MDB_PutChar(VMC_ACK);
}

void Send_Just_Reset(void)
{
    BLE_PutString("reset\r\n");
    BLE_PutString("just reset\r\n");
	MDB_PutChar(JUST_RESET);
	MDB_PutChar(VMC_ACK);
}


void Peripheral_ID_Send(void)
{
	
//	__delay_ms(1); __delay_us(800);
//	MDB_PutChar(0x009);   //Z1
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x056);   // Manufacturer 1st(Z2) code :V
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x04E);   // Manufacturer 2nd(Z3) code :N
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x04B);   // Manufacturer 3rd(Z4) code :K
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x030);   // Serial No Z5
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x030);   // Serial No Z6
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x030);   // Serial No Z7
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x030);   // Serial No Z8
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x030);   // Serial No Z9
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x030);   // Serial No Z10
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x031);   // Serial No Z11
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x030);   // Serial No Z12
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x032);   // Serial No Z13
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x030);   // Serial No Z14
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x032);   // Serial No Z15
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x031);   // Serial No Z16
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x056);   // Model  No Z17
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x045);   // Model  No Z18
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x04E);   // Model  No Z19
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x044);   // Model  No Z20
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x045);   // Model  No Z21
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x04B);   // Model  No Z22
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x049);   // Model  No Z23
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x04E);   // Model  No Z24
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x02D);   // Model  No Z25
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x030);   // Model  No Z26
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x037);   // Model  No Z27
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x031);   // Model  No Z28
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x030);   // Software vrn Z29
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(0x031);   // Software vrn Z30
//    __delay_ms(1);	__delay_us(800);
//
//	MDB_PutChar(0x1B8);   // CRC
    __delay_ms(1); __delay_us(800);
	MDB_PutChar(0x009);   //Z1
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0x056);   // Manufacturer 1st(Z2) code :V
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0x04E);   // Manufacturer 2nd(Z3) code :N
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0x04B);   // Manufacturer 3rd(Z4) code :K
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0x000);   // Serial No Z5
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X000);   // Serial No Z6
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X000);   // Serial No Z7
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X000);   // Serial No Z8
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X000);   // Serial No Z9
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X000);   // Serial No Z10
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X000);   // Serial No Z11
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X000);   // Serial No Z12
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X000);   // Serial No Z13
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X000);   // Serial No Z14
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X000);   // Serial No Z15
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X030);   // Serial No Z16
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X056);   // Model  No Z17  V
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X04E);   // Model  No Z18  N
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X04B);   // Model  No Z19  K
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X02D);   // Model  No Z20   "-"
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X030);   // Model  No Z21
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X030);   // Model  No Z22
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X030);   // Model  No Z23
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X030);   // Model  No Z24
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X030);   // Model  No Z25
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X030);   // Model  No Z26
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X030);   // Model  No Z27
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X034);   // Model  No Z28
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X000);   // Software vrn Z29
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X002);   // Software vrn Z30
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X000);   // Optional Feature bits Z31
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X000);   // Optional Feature bits Z32
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X002);   // Optional Feature bits Z33
	__delay_ms(1); __delay_us(800);
	MDB_PutChar(0X060);   //Optional Feature bits Z34
	__delay_ms(1); __delay_us(800);
	
	
	MDB_PutChar(0X12C);   // CRC
	

//	BLE_PutString("\r\n Sending Peripheral ID");

}

void Approved_Vend(void)
{
	MDB_PutChar(0x005);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(rev_amount_H);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(rev_amount_L);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(( VMC_ACK | (0x05+rev_amount_H+rev_amount_L)));
//	BLE_PutString("\r\nSend Vend Approved");
}

void Denied_Vend(void)
{
	__delay_ms(1);	__delay_us(800);
	MDB_PutChar(0x006);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(0x106);
//	BLE_PutString("\r\nSend Vend denied");
}


void Cancel_Session(void)
{
	MDB_PutChar(0x004);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(0x104);
//	BLE_PutString("\r\nSend Session Cancel Request");
}

void Vend_Cancelled(void)
{
	MDB_PutChar(0x008);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(0x108);

//	BLE_PutString("\r\nSend VMC Read Cancel Response ");
}

void Start_Session(void)
{
//	MDB_PutChar(0x003);
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(rev_amount_H);
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar(rev_amount_L);
//    __delay_ms(1);	__delay_us(800);
//	MDB_PutChar((0x100 | (0x03+rev_amount_H+rev_amount_L)));

//	BLE_PutString("\r\nSend MDB Session Started");
    
    unsigned int Sel_ChekSum=(0x14 + rev_amount_L + rev_amount_H + 0XFF + 0XFF + 0XFF + 0XFF + ItemNoMSB + ItemNoLSB);	
	
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(0X014);   //Z1  Selection Request
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(rev_amount_H);   //  Z2  Funds Avaiable  
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(rev_amount_L);   // Z3 Funds Avaiable 
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(0XFF);   // Z4 Payment Media ID
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(0XFF);   // Z5 Payment Media ID
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(0XFF);   // Z6 Payment Media ID
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(0XFF);   // Z7 Payment Media ID
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(0X00);   // Z8  Payment Type
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(0X00);   //  Z9 Payment Data
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(0X00);   //  Z10 Payment Data
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(ItemNoMSB);   //  Z11  Item No
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(ItemNoLSB);   //  Z12  Item No
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(0X00);   // Serial No Z13  Item  Options
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(0X00);   // Serial No Z14  Item  Options
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(0X00);   // Serial No Z15  Item  Options
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(0X00);   // Serial No Z16  Item  Options
	__delay_us(800);__delay_ms(1);
	MDB_PutChar(Sel_ChekSum|0x100);   //  CRC	
}


void End_Session(void)
{
//	BLE_PutString("\r\nSend MDB END Session ");
	MDB_PutChar(0x007);
    __delay_ms(1);	__delay_us(800);
	MDB_PutChar(0x107);
}

/*
  * calc_checksum()
  * Calculates checksum of *array from 0 to arr_size
  * Use with caution (because of pointer arithmetics)
  */
unsigned int calc_checksum(unsigned int *array, unsigned int arr_size)
{
	unsigned int ret_val = 0x00;
	unsigned int i;
	//__asm("NOP");
   	for(i=0;i<arr_size;i++)
    {
        ret_val += array[i];
    }
	//__asm("NOP");
	return ret_val;
}




