Hi

#include "basic_requirements.h"

//unsigned int guiBLE_RXBuffer_Index=0,guiBLE_Total_Frm_Len=0,guiExpectedLen=0;
//unsigned char gucBLE_Frm_rec_Flag=0,gucBLE_St_RcvData_Flag=0;

//unsigned char gucBLE_RXBuffer[32],gucBLE_TXBuffer[520];

unsigned char gucStar_buffer[32],gucDollor_buffer[5];
unsigned int guiStar_buff_index,guiDollor_buff_index;
unsigned char gucStarStringRCV_Flag,gucDollerStringRCV_Flag;

unsigned char gucBLE_ReCBuffer[32];

//unsigned char guAxpect_Subcode=0,gucResp=0,gucRept=0;

unsigned char gucVendkinAppConnected=0;

//unsigned char gucVendCancelReasone=0,gucBLEVendACK_Flag=0,gucDirectVendCancelFlag=0;

unsigned char gucCash_SalesData_Buff[2500] __at(0x200);

unsigned char gucRx_Product_Code[5];
unsigned char gucRx_Payment_Code[8];
unsigned char gucProduct_Price[6];
//unsigned char gucLastVendStatus;
unsigned char gucRevert_String[9];

unsigned int guiDex_Data_Total_Page=0,guiCurrentDexPage=0;

float gfinRs,gfinPaisa,gfBLEPrice;

unsigned int guiBLERecPrice;

unsigned char rev_amount_H,rev_amount_L,ItemNoMSB,ItemNoLSB;

unsigned char guc5SecIntFlag=0,gucStart5SecTimerFlag=0;
unsigned int gui5SecCounter=0;

unsigned int guiRelayPressCounter=0,guiSendResponseCounter=0;

unsigned char guc_HW_INfoSnd_OnceFLG=0,gucSendStatusToAPP=0,gucUpdateCashString=0,gucMachineReadyToVend;


unsigned int zLen=0;


void IsBLEConnected()
{
    if(dBLE_STATUS_PIN==0)
    {
        if(!guc_HW_INfoSnd_OnceFLG)
        {
            if((gucMDB_ReaderEnable_Flag) && (gucMachineReadyToVend))
            {
                BLE_PutString("\r\n");
                __delay_ms(100);
                
                gucVendStartFlag=0;
                        
                gucSendMachineReadyFlag=1;
                gucMachineReadySendCount=0;
                guiMachineReadyCounter=20000;
            }
            else
            {
                BLE_PutString("Machine Not Ready");
            }
                
            guc_HW_INfoSnd_OnceFLG=1;
            gucAPPConnected=1;
            
        }         
    }
    else
    {
        if(guc_HW_INfoSnd_OnceFLG)
        {
            BLE_PutString("App DisConnected\r\n ");
            guc_HW_INfoSnd_OnceFLG=0;
            gucAPPConnected=0;
            
            
            gucSendMachineReadyFlag=0;
            guiMachineReadyCounter=0;
            gucMachineReadySendCount=0;
            
            ucVendingTimerExpiredFlg=0;
            uc_VndReqstTimerFLG=0;
            ui_VndReqstTimerFCount=0;
        }
    }
}


void BLE_Communication()
{
    unsigned char key=0;
    
	if(gucStarStringRCV_Flag)
    {
//        BLE_PutString(gucStar_buffer);
        gucStarStringRCV_Flag=0;
        if ((gucStar_buffer[0]=='*') && (gucStar_buffer[29]=='*') && (gucVendStartFlag==0))
        {
            gucRelayPressCounter=0;
            gucStartRelayPressCounterFlag=0;
            gucCancel_Session=0;
            
            Clear_Key_Pressing();
                 
            uc_VndReqstTimerFLG=1;
            ui_VndReqstTimerFCount=0;
                                            
            gucVendStartFlag=1;
            gucSendMachineReadyFlag=0;
//            gucLastVendStatus= eeprom_read(1);
            // Payment 7digit code
             gucRx_Payment_Code[0]= gucStar_buffer[11];
             gucRx_Payment_Code[1]= gucStar_buffer[6];
             gucRx_Payment_Code[2]= gucStar_buffer[15];
             gucRx_Payment_Code[3]= gucStar_buffer[4];
             gucRx_Payment_Code[4]= gucStar_buffer[13];
             gucRx_Payment_Code[5]= gucStar_buffer[8];
             gucRx_Payment_Code[6]= gucStar_buffer[17];
             gucRx_Payment_Code[7]='\0';

          // product no	
            gucRx_Product_Code[0]= '0';
            gucRx_Product_Code[1]= gucStar_buffer[21];
            gucRx_Product_Code[2]= gucStar_buffer[22];
            gucRx_Product_Code[3]= gucStar_buffer[23];	
            gucRx_Product_Code[4]= '\0';
             
             
//             BLE_PutString("\r\n");
//             BLE_PutString(gucRx_Product_Code);
//             BLE_PutString("\r\n");

          // prouct price
             gucProduct_Price[0]= gucStar_buffer[24];
             gucProduct_Price[1]= gucStar_buffer[25];
             gucProduct_Price[2]= gucStar_buffer[26];
             gucProduct_Price[3]= gucStar_buffer[27];
             gucProduct_Price[4]= gucStar_buffer[28];
             gucProduct_Price[5]= '\0';
          // revert back

             //gucRevert_String[0]= gucStar_buffer[1];
             gucRevert_String[0]= 'D';
             gucRevert_String[1]= '9';
             gucRevert_String[2]= '9';
             gucRevert_String[3]= '9';
             gucRevert_String[4]= '9';
             gucRevert_String[5]= '9';
             gucRevert_String[6]=(gucLastVendStatus+0x30);
             gucRevert_String[7]= '\0';
            //Revet_strong[8]= '1';
//             BLE_PutString(gucRevert_String);
             int b;
             for (int j=0;j<=35;j++)
			{
                b=compare_strings(&Payment_Code[j][0],gucRx_Payment_Code);
				if (b==0)
                {
                    break;
                }
			}
            if (b==0)
			{
				
				BLE_PutString("ACK_ST\r\n");
				
			
					
				//rev_amount = atoi(product_price);	
				guiBLERecPrice = atof(gucProduct_Price);
				guiBLERecPrice=(unsigned int)((guiBLERecPrice-14));
                guiBLERecPrice = guiBLERecPrice *100;
				
				rev_amount_L=guiBLERecPrice & 0xFF;
				rev_amount_H=guiBLERecPrice>>8;

                gucRevert_String[7]='2';//keys are not press on machine 
                
//                Rcv_ProductNo_MSB=(gucStar_buffer[1])-0x30;
//                Rcv_ProductNo_MSB=((gucStar_buffer[1])-0x30)+1;
//                Rcv_ProductNo_LSB= 
                ItemNoMSB=((gucRx_Product_Code[1])-0x30)+1;;
				ItemNoLSB=(10*(gucRx_Product_Code[2]-0x30))+((gucRx_Product_Code[3]-0x30)+1);;
                
//				 __delay_ms(500);	
                 ucPresentMDB_Stage=1;
				 ucResPollStage = BEGIN_SESSION;
//                 BLE_PutString("start ssn \r\n");

			}
			else if (b!=0)
			{ 
				//all_off();

				gucRevert_String[7]= '7';
				BLE_PutString(gucRevert_String);
				__delay_ms(1);
				BLE_PutChar('0');
                
                BLE_PutString("\r\n");	
				__delay_ms(500);
				BLE_PutString("no vend case\r\n");
				__delay_ms(500);
				BLE_PutString("no vend case\r\n");
                __delay_ms(500);
				BLE_PutString("no vend case\r\n");
                __delay_ms(500);
                BLE_PutString("ssn complete\r\n");
                __delay_ms(500);
                BLE_PutString("end ssn\r\n");
                
                gucVendStartFlag=0;
			}
        }
        else if ((gucStar_buffer[0]!='*') && (gucStar_buffer[29]!='*') && (gucVendStartFlag==0))
        {
            BLE_PutString(gucStar_buffer);
            gucSendMachineReadyFlag=0;
            wrg_flg=1;
            
            for(int i=0;i<31;i++)
            {
                wrg_flg=1;
                gucStar_buffer[i]=0;	
            }
            BLE_PutString("wrg code");
            BLE_PutString("\r\n");	
            __delay_ms(20);
            BLE_PutString("527100150");
            BLE_PutString("\r\n");
            __delay_ms(27);
            
            BLE_PutString("no vend case\r\n");
				__delay_ms(500);
				BLE_PutString("no vend case\r\n");
                __delay_ms(500);
				BLE_PutString("no vend case\r\n");
                __delay_ms(500);
                BLE_PutString("ssn complete\r\n");
                __delay_ms(500);
                BLE_PutString("end ssn\r\n");
            gucVendStartFlag=0;
         gucSendReadEnableFlag=1;
         gucReadEnableSendCount=0;
         guiReadEnableCounter=20000;
            ucVendingTimerExpiredFlg=0;
            uc_VndReqstTimerFLG=0;
            ui_VndReqstTimerFCount=0;
         }
        guiStar_buff_index = 0;
        gucStar_buffer[guiStar_buff_index]=0;	
    }
    
    else if(gucDollerStringRCV_Flag)
    {
        gucDollerStringRCV_Flag=0;
//        BLE_PutString(gucDollor_buffer);
        for (int i=0;i<=9;i++)
        {

            char suc=compare_strings(Dollar_Array[i],gucDollor_buffer);
            if (suc==0)
            {
                switch(i)
                {
                    case(0): // Cash & EJB error
    
                                 for ( zLen=0;gucCash_SalesData_Buff[zLen]!='#';zLen++)
                                    {
                                        if(gucCash_SalesData_Buff[zLen]!='-')
                                        {
                                            BLE_PutChar(gucCash_SalesData_Buff[zLen]);
                                        }
                                        else if(gucCash_SalesData_Buff[zLen]=='-')
                                        {
                                            BLE_PutChar('-');
                                            zLen++;
                                            while(gucCash_SalesData_Buff[zLen]!='-')
                                            {
                                                zLen++;
                                            }
                                        }
                                    }
                                   BLE_PutChar('#');
                                   zLen++;
                                   for(;gucCash_SalesData_Buff[zLen]!=0x01;zLen++)
                                   {
                                       BLE_PutChar(gucCash_SalesData_Buff[zLen]);
                                   }
                                   BLE_PutString("\r\n");
                                   
//                                           BLE_PutString("\r\n zLen:- ");
//                                            BLE_PutChar((zLen/1000)+0x30);
//                                            BLE_PutChar(((zLen/100)%10)+0x30);
//                                            BLE_PutChar(((zLen/10)%10)+0x30);
//                                            BLE_PutChar((zLen%10)+0x30);
//                                            BLE_PutString("\r\n");
                                
                            break;

                    case(1): // Cash and soldout
                        
                                for ( zLen=0;gucCash_SalesData_Buff[zLen]!='#';zLen++)
                                {
                                    if(gucCash_SalesData_Buff[zLen]!='-')
                                    {
//                                        printf("%c ", arr[zLen]);
                                        BLE_PutChar(gucCash_SalesData_Buff[zLen]);
                                    }
                                    else if(gucCash_SalesData_Buff[zLen]=='-')
                                    {
//                                        printf("%c", '-');
                                        BLE_PutChar('-');
                                        zLen++;
                                        while(gucCash_SalesData_Buff[zLen]!='-')
                                        {
                                            zLen++;
                                        }
                                    }
                                }	
                                BLE_PutString("#S-*\r\n");
                                 break;
                    case(2)://cash & Temp  
                                
                                for ( zLen=0;gucCash_SalesData_Buff[zLen]!='#';zLen++)
                                {
                                    if(gucCash_SalesData_Buff[zLen]!='-')
                                    {
//                                        printf("%c ", arr[zLen]);
                                        BLE_PutChar(gucCash_SalesData_Buff[zLen]);
                                    }
                                    else if(gucCash_SalesData_Buff[zLen]=='-')
                                    {
//                                        printf("%c", '-');
                                        BLE_PutChar('-');
                                        zLen++;
                                        while(gucCash_SalesData_Buff[zLen]!='-')
                                        {
                                            zLen++;
                                        }
                                    }
                                }	
                                BLE_PutString("#T-*\r\n");
                                break;
                    case(3): // Cash & Motor Jam  Case
                        
                                for ( zLen=0;gucCash_SalesData_Buff[zLen]!='#';zLen++)
                                {
                                    if(gucCash_SalesData_Buff[zLen]!='-')
                                    {
//                                        printf("%c ", arr[zLen]);
                                        BLE_PutChar(gucCash_SalesData_Buff[zLen]);
                                    }
                                    else if(gucCash_SalesData_Buff[zLen]=='-')
                                    {
//                                        printf("%c", '-');
                                        BLE_PutChar('-');
                                        zLen++;
                                        while(gucCash_SalesData_Buff[zLen]!='-')
                                        {
                                            zLen++;
                                        }
                                    }
                                }	
                                BLE_PutString("#J-*\r\n");
                                break;

                    case(4)://  DEX initialization
                                if(!gucDex_init_flag)
                                {
                                   gucDollorD_Flag=1;
                                }
                                else
                                {
                                    BLE_PutString("DEX is already Running\r\n");
                                }
                               break;
                    case(5):// CC data
                            BLE_PutString("CC data\r\n");
                            key=Read_EEPROM(0X00);
                            __delay_ms(1);
//                              Complete_CC_Data();
                              break;
                    case(6):// Cash  Data with compartment price 
                            key=Read_EEPROM(0X00);
                            __delay_ms(1);
                            key=Read_EEPROM(0X00);
                            if(key==255)
                            {
                              BLE_PutString("DEX Not Initialized");
                            }
                            else
                            {										
//                            __delay_ms(5);
//                            SendStringSerially_BT("*");
//                            PdctArrwitPriceRd(0x20,key);
                                
                                BLE_PutString(gucCash_SalesData_Buff);
                                 BLE_PutString("\r\n");
                            }

//                            Uc_No_Page_Read=ReadByte_Frm_EEPROM(0X01);
//                            __delay_ms(1);
//                            Uc_No_Page_Read=ReadByte_Frm_EEPROM(0X01);	
//                            SendStringSerially_BT("#EJB-");
//                            EJB_Array_Read(0X1560,Uc_No_Page_Read);
//                            SendStringSerially_BT("*\r\n");
//
//                            Delay_ms(400);
//								SendStringSerially_BT("Machine Ready\r\n");
//								SendStringSerially_BT("ACK_ENABLE\r\n");
                            break;


                    case(7): // Ack for read eanable string 
                             gucSendReadEnableFlag=0;
                             gucReadEnableSendCount=0;
                             guiStar_buff_index = 0;
                              gucStar_buffer[guiStar_buff_index]=0;
                             BLE_PutString("RD\r\n");
                            break;
                    case(8):// Ack for Machine Readye string
                            gucSendMachineReadyFlag=0;
                            guiMachineReadyCounter=0;
                            break;                        
                    default: break;
                }
//                break;
            }
        }		
    }
}

void Debug_CovertChar_in_ASCIIwithLen(uint8_t *data,int len)
{
	unsigned char tempData,tempHex,tempHex1;
	while (len > 0)
	{
		tempData = *data++;
		tempHex=((tempData & 0xF0) >>4);
		tempHex1=((tempData & 0x0F));
		//			Hex_to_char(tempHex);   //convert the HEX to Character
		//			Hex_to_char(tempHex1);  //convert the Hex to Character
		//Debug_PutChar(Hex_to_char(tempHex));
		//Debug_PutChar(Hex_to_char(tempHex1));
		//Debug_PutChar(' ');
		BLE_PutChar(Hex_to_char(tempHex));
		BLE_PutChar(Hex_to_char(tempHex1));
		BLE_PutChar(' ');
		len--;
	}
}