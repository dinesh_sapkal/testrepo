/* 
 * File:   BLE_Protocol.h
 * Author: User
 *
 * Created on June 28, 2021, 12:23 PM
 */

#ifndef BLE_PROTOCOL_H
#define	BLE_PROTOCOL_H





extern unsigned int guiBLERecPrice;

extern unsigned char gucBLE_Frm_rec_Flag;
//extern unsigned char gucBLE_RXBuffer[32],gucBLE_TXBuffer[520];
extern unsigned char gucStar_buffer[32],gucDollor_buffer[5];
extern unsigned int guiStar_buff_index,guiDollor_buff_index;
extern unsigned char gucStarStringRCV_Flag,gucDollerStringRCV_Flag;

//extern unsigned int guiBLE_RXBuffer_Index,guiBLE_Total_Frm_Len,guiExpectedLen;

extern unsigned char guc5SecIntFlag,gucStart5SecTimerFlag;
extern unsigned int gui5SecCounter;

extern unsigned char rev_amount_H,rev_amount_L,ItemNoMSB,ItemNoLSB;

extern unsigned int guiRelayPressCounter,guiSendResponseCounter;

extern unsigned char guc_HW_INfoSnd_OnceFLG,gucSendStatusToAPP,gucUpdateCashString,gucMachineReadyToVend;
extern unsigned int guiMachineReadyCounter;

extern void IsBLEConnected(void);
extern void BLE_Communication(void);
extern void Debug_CovertChar_in_ASCIIwithLen(uint8_t *data,int len);

#endif	/* BLE_PROTOCOL_H */

