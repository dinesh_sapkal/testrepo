#include "basic_requirements.h"



const unsigned char Dollar_Array[][12]={
"$CH$", 	//0 cash & EJB
"$CS$", 	//1 Cash & soldout
"$CT$", 	//2 cash & Temp case7
"$CJ$", 	//3 cash & Motor Jam  Case 8
"$D$",  	//4 DEX initialization
"$CC$", 	//5 Cash, coin changer data
"$CP$",  	//6 cash datta with copartment price
"$ACK_RD$", //7 Ack for read eanable string 
"$ACK_EN$", //8 ack for cash data receive
};



unsigned int guiDex_Data_Total_Page=0,guiCurrentDexPage=0,guiCash_SalesData_Buff_index=0;
//unsigned char gucCash_SalesData_Buff[1500];


unsigned int Machinewait_Count=0;
unsigned char gucPowerOnDEX_Flag=1,gucDollorD_Flag=0,gucDex_init_flag=0,gucDex_ON_flag=0,gucDEX_Card_Reset_Status_Flg=0,gucDex_Running_flag=0;

//unsigned int guc_CMP_TempSale_CountLenBrforeConversion=0,guc_CMP_TempSale_CountLenAfterConversion=0;
unsigned int long guiTempSaleCount=0;

unsigned int xLen=0,yLen=0;
long unsigned int guiTempCompartmentPrice;
unsigned char gucCompartmentNotFound=0;
unsigned char guc_CMP_TempSale_Count_Before_Inc[9];
unsigned char guc_CMP_TempSale_Count_Before_Inc_Len;
unsigned char guc_CMP_TempSale_Count_After_Inc[9];
unsigned char guc_CMP_TempSale_Count_After_Inc_Len;
unsigned char guc_CMP_TempSale_Count[9];

unsigned char gucDEX_DoneFlag=0;

void DEX_Communication(void)
{
    unsigned char key=0;
    if((gucPowerOnDEX_Flag)&&(!gucMDB_ReaderEnable_Flag))
    {
        Machinewait_Count++;
        if(Machinewait_Count>5000)
        {
          Machinewait_Count=0;
          LED_7=~LED_7;	   
        }
    }
    
    if((gucPowerOnDEX_Flag||gucDollorD_Flag) && (gucMDB_ReaderEnable_Flag))
    {
        __delay_ms(1000);
        __delay_ms(1000);
        __delay_ms(1000);
        
        BLE_PutString("DEX On\r\n");

        
        gucDex_init_flag=1;
        DEX_Initialize_Pin=1;
        __delay_ms(1000);
        DEX_Initialize_Pin=0;
        __delay_ms(50);
        
        

        while(gucDex_init_flag)
        {

            IsBLEConnected();

            if((DEX_RUN_Pin==0)&&(!gucDex_ON_flag))
            {
                gucDex_ON_flag=1;
                LED_7=0;
            }

            if((DEX_RUN_Pin)&&(gucDex_ON_flag))
            {
                gucDEX_Card_Reset_Status_Flg=Read_EEPROM(0x08);
                __delay_ms(5);
                gucDEX_Card_Reset_Status_Flg=Read_EEPROM(0x08);
                __delay_ms(5);

                if(gucDEX_Card_Reset_Status_Flg)
                {
                    Write_EEPROM(0x08,0);
                    __delay_ms(5);
                    Write_EEPROM(0x08,0);
                    __delay_ms(5);
                    if(gucDex_init_flag)
                    {
                        BLE_PutString("DEX Not Complete\r\n");
                        BLE_PutString("DEX Not Initialized");
                    }

                    gucDEX_Card_Reset_Status_Flg=0;
                    LED_7=1;
                    gucDex_init_flag=0;
                }
                else
                {
                    if(gucDex_init_flag)
                    {
                        BLE_PutString("DEX Complete\r\n");
                        
                        gucDEX_DoneFlag=1;

                        key=Read_EEPROM(0x00);
                        __delay_ms(5);
                        key=Read_EEPROM(0x00);
                        if(key==255)
                        {
                            BLE_PutString("DEX Not Initialized");
                        }
                        else
                        {
                             __delay_ms(5);

                            guiCash_SalesData_Buff_index=0;
                            gucCash_SalesData_Buff[guiCash_SalesData_Buff_index] ='*';
//                                BLE_PutChar(gucCash_SalesData_Buff[guiCash_SalesData_Buff_index]);	
                            guiCash_SalesData_Buff_index++;
                            Product_Array_Read(0x20,key);
//                            __delay_ms(5);

                            unsigned char Uc_No_Page_Read=0;

                            Uc_No_Page_Read=Read_EEPROM(0x01);	
//                            BLE_PutString("#EJB-");
                            gucCash_SalesData_Buff[guiCash_SalesData_Buff_index] ='#';
//                                BLE_PutChar(gucCash_SalesData_Buff[guiCash_SalesData_Buff_index]);	
                            guiCash_SalesData_Buff_index++;
                            gucCash_SalesData_Buff[guiCash_SalesData_Buff_index] ='E';
//                                BLE_PutChar(gucCash_SalesData_Buff[guiCash_SalesData_Buff_index]);	
                            guiCash_SalesData_Buff_index++;
                            gucCash_SalesData_Buff[guiCash_SalesData_Buff_index] ='J';
//                                BLE_PutChar(gucCash_SalesData_Buff[guiCash_SalesData_Buff_index]);	
                            guiCash_SalesData_Buff_index++;
                            gucCash_SalesData_Buff[guiCash_SalesData_Buff_index] ='B';
//                                BLE_PutChar(gucCash_SalesData_Buff[guiCash_SalesData_Buff_index]);	
                            guiCash_SalesData_Buff_index++;
                            gucCash_SalesData_Buff[guiCash_SalesData_Buff_index] ='-';
//                                BLE_PutChar(gucCash_SalesData_Buff[guiCash_SalesData_Buff_index]);	
                            guiCash_SalesData_Buff_index++;
                            //EJB_Array_Read(0x1560,Uc_No_Page_Read);
                            EJB_Array_Read(0x0F00,Uc_No_Page_Read);
//                            BLE_PutString("\r\n");	
                            gucCash_SalesData_Buff[guiCash_SalesData_Buff_index] ='*';
//                                BLE_PutChar(gucCash_SalesData_Buff[guiCash_SalesData_Buff_index]);	
                            guiCash_SalesData_Buff_index++;
                            
//                            BLE_PutString("\r\n");	

                            gucCash_SalesData_Buff[guiCash_SalesData_Buff_index]=0x01;//this is end byte of cash string
//                                BLE_PutChar(gucCash_SalesData_Buff[guiCash_SalesData_Buff_index]);	
                           guiCash_SalesData_Buff_index++;
                           
                            gucCash_SalesData_Buff[guiCash_SalesData_Buff_index] ='\0';//this is end byte of cash string
                           guiCash_SalesData_Buff_index++;
                           
                           //------------------- Send DEx data to BLE ----------------------------------------
                           for ( xLen=0;gucCash_SalesData_Buff[xLen]!='#';xLen++)
                            {
                                if(gucCash_SalesData_Buff[xLen]!='-')
                                {
                                    BLE_PutChar(gucCash_SalesData_Buff[xLen]);
                                }
                                else if(gucCash_SalesData_Buff[xLen]=='-')
                                {
                                    BLE_PutChar('-');
                                    xLen++;
                                    while(gucCash_SalesData_Buff[xLen]!='-')
                                    {
                                        xLen++;
                                    }
                                }
                            }
                           BLE_PutChar('#');
                           xLen++;
                           for(;gucCash_SalesData_Buff[xLen]!=0x01;xLen++)
                           {
                               BLE_PutChar(gucCash_SalesData_Buff[xLen]);
                           }
                           BLE_PutString("\r\n");
                           gucMachineReadyToVend=1;
                        }
//                        __delay_ms(100);
                        gucDex_init_flag=0; 
                        LED_7=1;
                    }
                }
                gucDex_ON_flag=0;
                gucDollorD_Flag=0;
                gucDex_Running_flag=0;
            } 
        }
        gucPowerOnDEX_Flag=0; 
        gucMachineReadyToVend=1;
    }
}



void Product_Array_Read(unsigned int Strat_Add, unsigned char No_Page_Read)
{
    unsigned char f=0,ReadByte=0,i;
    unsigned int   Page_Add=0;
    unsigned char RD_StarCount=0;
    
    
    
    Page_Add=Strat_Add;
    for(i=0;i<No_Page_Read;i++)
    {
        Strat_Add=Page_Add;
		for(f=0;f<32;f++)			
        {
            i2c_start(I2C_START);													
//            while(i2c_write(0xa0) == 1)// Wait until device is free
//            {	i2c_start(I2C_REP_START);	}
            i2c_write(0xa0);
            i2c_write(Strat_Add>>8);
            i2c_write((unsigned char)Strat_Add);
            i2c_start(I2C_REP_START);
            i2c_write(0xa1);
            ReadByte=i2c_read();
            i2c_master_ack(I2C_DATA_NACK);
            __delay_ms(5);
            i2c_stop();
             
            if(ReadByte=='@')
            {
                Page_Add=Page_Add+32;
                RD_StarCount=0;
                if((i+2)<=No_Page_Read)
                {
                    gucCash_SalesData_Buff[guiCash_SalesData_Buff_index] = ReadByte;
//                    BLE_PutChar(guiCash_SalesData_Buff[guiCash_SalesData_Buff_index]);	
                    guiCash_SalesData_Buff_index++;
                }
                 break;
            }
            else
            {
                if(ReadByte=='*')
                {
                    gucCash_SalesData_Buff[guiCash_SalesData_Buff_index] = '-';
//                    BLE_PutChar(guiCash_SalesData_Buff[guiCash_SalesData_Buff_index]);	
                    guiCash_SalesData_Buff_index++;
                }
                else
                {
                    gucCash_SalesData_Buff[guiCash_SalesData_Buff_index] = ReadByte;
//                    BLE_PutChar(guiCash_SalesData_Buff[guiCash_SalesData_Buff_index]);	
                    guiCash_SalesData_Buff_index++;
                }
            }
            Strat_Add++;
        }  
    }
}


void EJB_Array_Read(unsigned int Strat_Add,unsigned char No_Of_Page)
{
    unsigned char f=0,ReadByte=0,i;
    unsigned int   Page_Add=0;
    Page_Add=Strat_Add;
    for(i=0;i<No_Of_Page;i++)
    {
        Strat_Add=Page_Add;
        for(f=0;f<32;f++)			
        {
            i2c_start(I2C_START);															
//            while(i2c_write(0xa0) == 1)// Wait until device is free
//            {	i2c_start(I2C_REP_START);	}
            i2c_write(0xa0);
            i2c_write(Strat_Add>>8);
            i2c_write((unsigned char)Strat_Add);
            i2c_start(I2C_REP_START);
            i2c_write(0xa1);
            ReadByte=i2c_read();
            i2c_master_ack(I2C_DATA_NACK);
            __delay_ms(5);
            i2c_stop();
            
            if(ReadByte=='\0')
            {
                Page_Add=Page_Add+32;
                break;
            }
            else if(ReadByte=='*')
            {
                if(i<(No_Of_Page-1))
                {
                    gucCash_SalesData_Buff[guiCash_SalesData_Buff_index] = '@';
//                    BLE_PutChar(guiCash_SalesData_Buff[guiCash_SalesData_Buff_index]);	
                    guiCash_SalesData_Buff_index++;
                }
            }
            else
            {
                    gucCash_SalesData_Buff[guiCash_SalesData_Buff_index] = ReadByte;
//                    BLE_PutChar(guiCash_SalesData_Buff[guiCash_SalesData_Buff_index]);	
                    guiCash_SalesData_Buff_index++;
            }
            Strat_Add++;
        }
    }
}


unsigned int toint(unsigned char str[],int slen)
{
	int len = slen;
	unsigned int i, num = 0;
	
	for (i = 0; i < len; i++)
	{
		num = num + ((str[len - (i + 1)]) * pow(10, i));
	}
	return num;
}

void tostring(char str[], int num)
{
	int i, rem, len = 0, n;
	
	n = num;
	while (n != 0)
	{
		len++;
		n /= 10;
	}
	for (i = 0; i < len; i++)
	{
		rem = num % 10;
		num = num / 10;
		str[len - (i + 1)] = rem + '0';
	}
	str[len] = '\0';
	guc_CMP_TempSale_Count_After_Inc_Len=len;
}


void Calculate_Compartment_Number()
{
    for(int xz=0;xz<=9;xz++)
    {
        guc_CMP_TempSale_Count_Before_Inc[xz]='\0';
    }
    for (xLen=0;gucCash_SalesData_Buff[xLen]!='#';xLen++)
    {
        if ((gucVMCRec_CMP_NUM[0]==(gucCash_SalesData_Buff[xLen]-0x30)) &&
        (gucVMCRec_CMP_NUM[1]==(gucCash_SalesData_Buff[xLen+1]-0x30)) &&
        (gucVMCRec_CMP_NUM[2]==(gucCash_SalesData_Buff[xLen+2]-0x30)))
        {
//            BLE_PutString("\r\n Compartment Number Found/Matched for TempSaleCount");
            xLen=xLen+3;

            if (gucCash_SalesData_Buff[xLen]=='-')
            {
                xLen++;
                for (;gucCash_SalesData_Buff[xLen]!='-';xLen++)
                {

                }
            }
            if (gucCash_SalesData_Buff[xLen]=='-')
            {
                xLen++;
                for (yLen=0;gucCash_SalesData_Buff[xLen]!='@';xLen++)
                {
                    guc_CMP_TempSale_Count_Before_Inc[yLen]=(gucCash_SalesData_Buff[xLen])-0x30;
//                    BLE_PutChar(guc_CMP_TempSale_Count[yLen]+0x30);
                    yLen++;
                }
            }
        }
    }
    guc_CMP_TempSale_Count_Before_Inc_Len=yLen;
//    guc_CMP_TempSale_Count_Before_Inc_Len = strlen(guc_CMP_TempSale_Count_Before_Inc);
//    for(int xu=0;guc_CMP_TempSale_Count_Before_Inc[xu]!='\0';xu++)
//    {
//        guc_CMP_TempSale_Count_Before_Inc_Len++;
//    }
//    guc_CMP_TempSale_Count_Before_Inc_Len=guc_CMP_TempSale_CountLenBrforeConversion;
//    BLE_PutString("\r\nguc_CMP_TempSale_Count_Before_Inc:- ");
//    BLE_PutChar((guc_CMP_TempSale_Count_Before_Inc_Len/1000)+0x30);
//    BLE_PutChar(((guc_CMP_TempSale_Count_Before_Inc_Len/100)%10)+0x30);
//    BLE_PutChar(((guc_CMP_TempSale_Count_Before_Inc_Len/10)%10)+0x30);
//    BLE_PutChar((guc_CMP_TempSale_Count_Before_Inc_Len%10)+0x30);
    
    guiTempSaleCount=0;
    guiTempSaleCount = toint(guc_CMP_TempSale_Count_Before_Inc,guc_CMP_TempSale_Count_Before_Inc_Len);
//    BLE_PutString("\r\n Temp Sales Count:- ");
//    BLE_PutChar((guiTempSaleCount/1000)+0x30);
//    BLE_PutChar(((guiTempSaleCount/100)%10)+0x30);
//    BLE_PutChar(((guiTempSaleCount/10)%10)+0x30);
//    BLE_PutChar((guiTempSaleCount%10)+0x30);
//    BLE_PutString("\r\n");
}



void LiveCashDataUpdate()
{
    unsigned int totalPos,pos,bufferLen,i;
    unsigned char x=0;
    
    bufferLen = strlen(gucCash_SalesData_Buff);
//	BLE_PutString("live_Updating\r\n");

    guiTempSaleCount++;
    for(int xy=0;xy<=9;xy++)
    {
        guc_CMP_TempSale_Count_After_Inc[xy]='\0';
    }
    tostring(guc_CMP_TempSale_Count_After_Inc,guiTempSaleCount);

    
//    guc_CMP_TempSale_Count_After_Inc_Len = strlen(guc_CMP_TempSale_Count_After_Inc);
    
    
    
//    BLE_PutString("\r\nguc_CMP_TempSale_Count_After_Inc:- ");
//    BLE_PutChar((guc_CMP_TempSale_Count_After_Inc_Len/1000)+0x30);
//    BLE_PutChar(((guc_CMP_TempSale_Count_After_Inc_Len/100)%10)+0x30);
//    BLE_PutChar(((guc_CMP_TempSale_Count_After_Inc_Len/10)%10)+0x30);
//    BLE_PutChar((guc_CMP_TempSale_Count_After_Inc_Len%10)+0x30);
    
    //if(strlen(guc_CMP_TempSale_Count_Before_Inc) == strlen(guc_CMP_TempSale_Count_After_Inc))
    if(guc_CMP_TempSale_Count_Before_Inc_Len == guc_CMP_TempSale_Count_After_Inc_Len)
    {
//        BLE_PutString("\r\n if(guc_CMP_TempSale_Count_Before_Inc_Len == guc_CMP_TempSale_Count_After_Inc_Len)");
        for ( xLen=0;gucCash_SalesData_Buff[xLen]!='#';xLen++)
    	{
            if ((gucVMCRec_CMP_NUM[0]==(gucCash_SalesData_Buff[xLen]-0x30)) &&
                (gucVMCRec_CMP_NUM[1]==(gucCash_SalesData_Buff[xLen+1]-0x30)) &&
                (gucVMCRec_CMP_NUM[2]==(gucCash_SalesData_Buff[xLen+2]-0x30)))
    		{
    			//Debug_PutString("\r\n Compartment Number Found/Matched for TempSaleCount");
    			xLen=xLen+3;
    			
    			if (gucCash_SalesData_Buff[xLen]=='-')
    			{
    				xLen++;
    				for (;gucCash_SalesData_Buff[xLen]!='-';xLen++)
    				{
    					
    				}
    			}
    			if (gucCash_SalesData_Buff[xLen]=='-')
    			{
    				xLen++;
    				for (yLen=0;yLen<strlen(guc_CMP_TempSale_Count_After_Inc);xLen++)
    				{
    					//guc_CMP_TempSale_Count_Before_Inc[yLen]=(gucCash_SalesData_Buff[xLen]);
    					gucCash_SalesData_Buff[xLen] = guc_CMP_TempSale_Count_After_Inc[yLen];
    				// 	printf("\n");
    				// 	printf("%c ", guc_CMP_TempSale_Count_Before_Inc[yLen]);
    					yLen++;
    				}
    				break;
    			}
    		}
    // 		break;
    	}
    }
    //else if((strlen(guc_CMP_TempSale_Count_After_Inc) - strlen(guc_CMP_TempSale_Count_Before_Inc))>=1)
    else if((guc_CMP_TempSale_Count_After_Inc_Len - guc_CMP_TempSale_Count_Before_Inc_Len)>=1)
    {
//        BLE_PutString("\r\n else if((guc_CMP_TempSale_Count_After_Inc_Len - guc_CMP_TempSale_Count_Before_Inc_Len)>=1)");
//        printf("Please write Logic\n");
        totalPos = (strlen(guc_CMP_TempSale_Count_After_Inc)) - 
                    (strlen(guc_CMP_TempSale_Count_Before_Inc));
//        printf("Total Change Position:- ");
//        printf("%d ", totalPos);
        
        for ( xLen=0;gucCash_SalesData_Buff[xLen]!='#';xLen++)
    	{
    		if ((gucVMCRec_CMP_NUM[0]==(gucCash_SalesData_Buff[xLen]-0x30)) &&
                (gucVMCRec_CMP_NUM[1]==(gucCash_SalesData_Buff[xLen+1]-0x30)) &&
                (gucVMCRec_CMP_NUM[2]==(gucCash_SalesData_Buff[xLen+2]-0x30)))
    		{
    			xLen=xLen+3;
    			
    			if (gucCash_SalesData_Buff[xLen]=='-')
    			{
    				xLen++;
    				for (;gucCash_SalesData_Buff[xLen]!='-';xLen++)
    				{
    					
    				}
    			}
                if (gucCash_SalesData_Buff[xLen]=='-')
                {
                	xLen++;
                	//for (yLen=0;yLen<=(strlen(guc_CMP_TempSale_Count_After_Inc));xLen++)
                    
                    for (yLen=0;yLen<=(guc_CMP_TempSale_Count_After_Inc_Len);xLen++)
                	{
                		// element to be inserted
                		x = guc_CMP_TempSale_Count_After_Inc[yLen];
                		// position at which element
                		// is to be inserted
                		pos = xLen+1;
                		// increase the size by 1
                			bufferLen++;
                		// shift elements forward
                		if(yLen==0)
                		{
                			for (i = bufferLen-1; i >= pos+1; i--)
                			{
                			   // printf("\n");
                			   // printf("%c",gucCash_SalesData_Buff[i - 1]);
                				gucCash_SalesData_Buff[i] = gucCash_SalesData_Buff[i - 1];
                			}
                		}
                			
                		// insert x at pos
                		gucCash_SalesData_Buff[pos - 1] = x;
                		
                	 	//gucCash_SalesData_Buff[xLen] = guc_CMP_TempSale_Count_After_Inc[yLen];
                		yLen++;
                	}
                	gucCash_SalesData_Buff[i+((guc_CMP_TempSale_Count_After_Inc_Len)-1)] = '@';
                	break;
                }
    		}
    	}
    }
    
//    BLE_PutString("\r\n After Updating:- ");
//	BLE_PutString(gucCash_SalesData_Buff);
//    __delay_ms(500);
}