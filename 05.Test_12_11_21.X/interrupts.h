/* 
 * File:   interrupts.h
 * Author: User
 *
 * Created on June 28, 2021, 12:51 PM
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H


#define   MaxChars	31

void __interrupt() INTERRUPT_InterruptManager (void);

#endif	/* INTERRUPTS_H */

