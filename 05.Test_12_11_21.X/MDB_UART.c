#include "basic_requirements.h"


void initilize_MDB_uart(void) 
{
     TRISC6 = 0;   					// TX Pin
	TRISC7 = 1;   					// RX Pin
     // disable interrupts before changing states
    PIE1bits.RC1IE = 0;
   // ABDOVF no_overflow; CKTXP async_noninverted_sync_fallingedge; BRG16 16bit_generator; WUE disabled; ABDEN disabled; DTRXP not_inverted; 
    BAUDCON1 = 0x08;

    // SPEN enabled; RX9 9-bit; CREN enabled; ADDEN disabled; SREN disabled; 
    RCSTA1 = 0xD0;

    // TX9 9-bit; TX9D 0; SENDB sync_break_complete; TXEN enabled; SYNC asynchronous; BRGH hi_speed; CSRC slave_mode; 
    TXSTA1 = 0x64;

    // 
    SPBRG1 = 0x1F;

    // 
    SPBRGH1 = 0x01;
    
    PIE1bits.RC1IE = 1;
}

void MDB_PutChar(unsigned int sdata) 
{
    unsigned char temp=0;
    temp = (sdata & 0x100)>>8;
    TX9D1 = temp;
    PIR1bits.TX1IF=0;
    while(0 == PIR1bits.TX1IF)
    {
    }

    TXREG1 = (unsigned char)sdata;    // Write the data byte to the USART.
}

//void MDB_PutString(char *str) 
//{
//	unsigned int i=0;
//    while(str[i]!='\0')                 //check for NULL character to terminate loop
//    {
//        PIR1bits.TX1IF=0;
//        while(0 == PIR1bits.TX1IF);
//        TXREG1 = str[i];                     // Load data onto buffer
//        i++;
//    }
//}

//void MDB_PutStringL(unsigned char *str,unsigned int len)
//{
//	 unsigned int i=0;
//    while(len>0)
//    {
//        PIR1bits.TX1IF=0;
//        while(0 == PIR1bits.TX1IF)
//        {
//        }
//        TXREG1 = str[i];        
//        len--;
//        i++; 
//    }
//}
//
///* read a character from UART3 */
//unsigned char MDB_recieve_uart(void) 
//{
//	char c=0;
//
////	while(!(REG_SERCOM2_USART_INTFLAG & 4)) {}  /* wait until receive buffer is full */
////	c = REG_SERCOM2_USART_DATA;                 /* read the receive char */
//	return c;
//}



