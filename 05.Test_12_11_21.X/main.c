/*
 * File:   main.c
 * Author: User
 *
 * Created on June 28, 2021, 11:49 AM
 */


#include "basic_requirements.h"

unsigned int guiLED_Counter;
unsigned char gucAPPConnected=0,gucSendMachineReadyFlag=0,gucMachineReadySendCount=0,wrg_flg=0;
unsigned char gucSendReadEnableFlag=0,gucReadEnableSendCount=0,gucReadenableWaitFlag=0,gucReadenableWaitCounter=0;
unsigned int guiMachineReadyCounter=0,guiReadEnableCounter=0;
 

void main(void) {
    
    
    pin_initilize();
    dBLE_RESET_PIN=0x01;
    __delay_ms(30);
    
    LED_5=1; // LED OFF
    
    initilize_MDB_uart();
    initilize_BLE_uart();
    BLE_PutString("Hi Vendekin \r\n");
    //BLE_PutString("Dinesh \r\n");
    TMR0_Initialize();
    i2c_init();

    INTCONbits.PEIE = 1;        //enable global interrupt
    INTCONbits.GIE = 1; 
    
    gucMDB_Step=0;	
    TMR0_StartTimer();
    
    gucMachineReadyToVend=0;
//-----------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int h=0;h<500;h++)
//    for(unsigned int h=0;h<5;h++)
	{	
		 __delay_ms(500);
		 LED_7=~LED_7;
         IsBLEConnected();
	 	
	}
//-----------------------------------------------------------------------------------------------------------------------------------------
    
//    gucPowerOnDEX_Flag=1;
//    gucMachineReadyToVend=0;
    gucPowerOnDEX_Flag=1;
//    gucMachineReadyToVend=0;
    
    //ACK_Stage_State = ACK_RCV_JUST_RESET;
    MDB_NEXT_POLL_STATE = JUST_RESET;
   gucLastVendStatus=0;
    
   BLE_PutString("while start\r\n");
    while (1)
    {
        guiLED_Counter++;
        if ((guiLED_Counter>=1000) && (gucPowerOnDEX_Flag==0) && (gucMDB_ReaderEnable_Flag==1))
        {
            TEST_LED = ~ TEST_LED;
            guiLED_Counter=0;
        }
        
        if((gucSendMachineReadyFlag==1) && (gucMachineReadySendCount<5) && (gucVendStartFlag==0))
        {
            guiMachineReadyCounter++;
            if(guiMachineReadyCounter>20000)
            {
                guiMachineReadyCounter=0;
                BLE_PutString("Machine Ready\r\n");
                BLE_PutString("ACK_ENABLE\r\n");
                gucMachineReadySendCount++;  
            }
        }
        else if((gucSendReadEnableFlag==1) && (gucReadEnableSendCount<2) && (gucVendStartFlag==0))
        {
            guiReadEnableCounter++;
            if(guiReadEnableCounter>20000)
            {
                guiReadEnableCounter=0;
                BLE_PutString("read enable\r\n");
                gucReadEnableSendCount++;  
                
                gucReadenableWaitFlag=0;
                gucReadenableWaitCounter=0;
            }
        }
        IsBLEConnected();
        DEX_Communication();
        MDB_Communication();
		BLE_Communication();
	}
}
