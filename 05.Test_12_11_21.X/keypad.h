/* 
 * 
 * File:   keypad.h
 * Author: User
 *
 * Created on June 28, 2021, 3:00 PM
 */

#ifndef KEYPAD_H
#define	KEYPAD_H


#define  R1C1_RELAY		LATB7
#define  R1C2_RELAY		LATA1
#define  R1C3_RELAY		LATB2


#define  R2C1_RELAY		LATB3
#define  R2C2_RELAY		LATA4
#define  R2C3_RELAY		LATA5

#define  R3C1_RELAY		LATE0
#define  R3C2_RELAY		LATE1
#define  R3C3_RELAY		LATE2


#define  R4C1_RELAY		LATC0
#define  R4C2_RELAY		LATC1
#define  R4C3_RELAY		LATC2


//#define  R1C4_RELAY		LATC0
//#define  R2C4_RELAY		LATC1
//#define  R3C4_RELAY		LATC2
//#define  R4C4_RELAY		KEY25


#define KEY_1_TCN	    		LATB7  //Relay_C1R1
#define KEY_2_TCN    			LATA1  //Relay_C2R1 
#define KEY_3_TCN    			LATB2  //Relay_C3R1

#define KEY_4_TCN    			LATB3  //Relay_C1R2 
#define KEY_5_TCN    			LATA4  //Relay_C2R2 
#define KEY_6_TCN    			LATA5  //Relay_C3R2
 

#define KEY_7_TCN    			LATE0  //Relay_C1R3 
#define KEY_8_TCN    			LATE1  //Relay_C2R3
#define KEY_9_TCN    			LATE2  //Relay_C3R3 

#define KEY_DOT_TCN   			LATB6   //Relay_C1R4
#define KEY_0_TCN	    		LATC1   //Relay_C2R4
#define KEY_CNL_TCN	    		LATB0  //Relay_C3R4

extern unsigned char ucMatched_Row,ucMatched_Col;
extern unsigned char ucKeyPressMask_Flg,ucKey_DelayTimerSRT_FLG,ucKeyEND_FLG;
extern unsigned int uiKey_DelayTimerCount;
extern volatile unsigned char ucKey_Sqnce_Number;


extern unsigned char gucRx_Product_Code[5];
extern unsigned char gucRx_Payment_Code[8];
extern unsigned char gucProduct_Price[6];
//extern unsigned char gucLastVendStatus;
extern unsigned char gucRevert_String[9];

extern const unsigned char Procudt_Code[72][6];
 
extern const unsigned char Payment_Code[36][8];
unsigned int compare_strings(const unsigned char a[], unsigned char b[]);
extern void motor_delay(void);
extern void motor_Btn_delay(void);
extern void PressMachineKey(unsigned char motor_num);


void Start_Key_Pressing(void);
void Clear_Key_Pressing(void);
void Press_Keys(unsigned char Key_posi_Num);
void Compare_2D_Array( unsigned char value);
void Pressed_Matrix_KeyON(unsigned char Row,unsigned char col);
void Pressed_Matrix_KeyOFF(unsigned char Row,unsigned char col);

unsigned char Hex_to_char(unsigned char temp);
#endif	/* KEYPAD_H */

